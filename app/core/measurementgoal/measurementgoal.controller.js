(function () { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name MeasurementGoalController
* @module metricapp
* @requires $scope
* @requires $location
* @requires MeasurementGoalService
* @requires MetricService
* @requires $window
* @requires AuthService
* @requires STATES
* @requires MetricModalService
* @requires ContextFactorService
* @requires AssumptionService
* @requires ContextFactorModalService
* @requires AssumptionModalService
* @requires $routeParams
* @requires QuestionStorageFactory
* @requires QuestionModalFactory
* @description
* Manages the MeasurementGoal with read and update functionality.
* Realizes the control layer for `measurementgoal.view`.
************************************************************************************/
angular.module('metricapp')

.controller('MeasurementGoalController', MeasurementGoalController);

MeasurementGoalController.$inject = ['$scope', '$location','MeasurementGoalService','MetricService','$window','AuthService', 'STATES', 'GraphService', 'MetricModalService', 'ContextFactorService', 'AssumptionService', 'ContextFactorModalService', 'AssumptionModalService', '$routeParams', 'QuestionStorageFactory', 'QuestionModalFactory', 'StateMachineMeasurementGoalService', 'UserService'];

function MeasurementGoalController($scope, $location, MeasurementGoalService, MetricService, $window, AuthService, STATES, GraphService, MetricModalService, ContextFactorService, AssumptionService, ContextFactorModalService, AssumptionModalService, $routeParams, QuestionStorageFactory, QuestionModalFactory, StateMachineMeasurementGoalService, UserService) {

    var vm = this;

    //Initialize some transition variables
    vm.externalMetricDialog = [];
    vm.externalContextFactorDialog = [];
    vm.externalAssumptionDialog = [];

    vm.submitMeasurementGoal = submitMeasurementGoal;
    vm.goToUpdateMeasurementGoal = goToUpdateMeasurementGoal;
    vm.addTagToMeasurementGoal = addTagToMeasurementGoal;
    vm.removeTagFromMeasurementGoal = removeTagFromMeasurementGoal;
    vm.getApprovedMetrics = getApprovedMetrics;
    vm.removeSomethingFromMeasurementGoal = removeSomethingFromMeasurementGoal;
    vm.isModifiable = isModifiable;
    vm.getExternalContextFactors = getExternalContextFactors;
    vm.getExternalAssumptions = getExternalAssumptions;
    vm.getMetricsToUpdate = getMetricsToUpdate;
    vm.setMetricDialog = setMetricDialog;
    vm.setQuestionDialog = setQuestionDialog;
    vm.setPQuestionDialog = setPQuestionDialog;
    vm.getContextFactorsToUpdate = getContextFactorsToUpdate;
    vm.getAssumptionsToUpdate = getAssumptionsToUpdate;
    vm.setContextFactorDialog = setContextFactorDialog;
    vm.setAssumptionDialog = setAssumptionDialog;
    vm.commitChangeState = commitChangeState;
    vm.isForApproval = isForApproval;
    vm.isToApprove = isToApprove;
    vm.isToChangeRequest = isToChangeRequest;
    vm.isToReject = isToReject;
    vm.isToAssign = isToAssign;
    vm.noMetricatorsForMG = noMetricatorsForMG;
    vm.noQuestionersForMG = noQuestionersForMG;
    vm.isToAddQuestioner = isToAddQuestioner;
    vm.changeState = changeState;

    _init();
    _selectMeasurementGoalToView();

    /********************************************************************************
    * @ngdoc method
    * @name _initMeasurementGoalDialog
    * @description
    * This function initializes a Measurement Goal from MeasurementGoalService
    ********************************************************************************/
    function _initMeasurementGoalDialog(){
        vm.measurementGoalDialog = MeasurementGoalService.getUpdateMeasurementGoal().measurementGoal;
        vm.metrics = MeasurementGoalService.getUpdateMeasurementGoal().metrics;
        vm.contextFactors = MeasurementGoalService.getUpdateMeasurementGoal().contextFactors;
        vm.assumptions = MeasurementGoalService.getUpdateMeasurementGoal().assumptions;
        vm.organizationalGoal = MeasurementGoalService.getUpdateMeasurementGoal().organizationalGoal;
        vm.instanceProject = MeasurementGoalService.getUpdateMeasurementGoal().instanceProject;        
        vm.questions = MeasurementGoalService.getUpdateMeasurementGoal().questions;
        vm.pendingQuestions = MeasurementGoalService.getUpdateMeasurementGoal().pendingQuestions;
    }

    /********************************************************************************
    * @ngdoc method
    * @name _selectMeasurementGoalToView
    * @description
    * This function checks that in the url there's param id.
    * If id is specified, the page is loaded with the Measurement Goal id specified.
    * Otherwise page is loaded with Measurement Goal in MeasurementGoalService
    ********************************************************************************/
    function _selectMeasurementGoalToView(){
        if (!$routeParams.measurementgoalid) {
            //Can modify
            vm.modifying = true;
            _initMeasurementGoalDialog();
        }
        else {
            //Only for readers
            vm.modifying = false;

            vm.currMeasurementGoalId = {
                id: $routeParams.measurementgoalid
            };

            //Retrieve Measurement Goal Details
            MeasurementGoalService.getMeasurementGoalsBy(vm.currMeasurementGoalId.id,'id').then(
                function(data){
                    vm.measurementGoalDialog = data.measurementGoals[0];
                    
                    //Retrieve Measurement Goal Externals
                    MeasurementGoalService.getMeasurementGoalExternals(vm.currMeasurementGoalId.id).then(
                        function (response) {
                            console.log('Success in read to get Measurement Goal Externals');
                          //  console.log(response);

                            vm.metrics = response.metrics;
                            vm.contextFactors = response.contextFactors;
                            vm.assumptions = response.assumptions;
                            vm.organizationalGoal = response.organizationalGoal;
                            vm.instanceProject = response.instanceProject;
                            vm.questions = response.questions;
                            vm.pendingQuestions = response.pendingQuestions;

                            var toUpdate = {
                                measurementGoal : vm.measurementGoalDialog,
                                metrics : response.metrics,
                                contextFactors : response.contextFactors,
                                assumptions : response.assumptions,
                                organizationalGoal : response.organizationalGoal,
                                instanceProject : response.instanceProject
                            };
                            MeasurementGoalService.toUpdateMeasurementGoal(toUpdate);
                        },
                        function (response) {
                            console.log('Failure get Measurement Goal Externals');
                            console.log(response);
                        }
                    );
                },function(data){
                    vm.error = true;
                }
            );
        }
    }

    /********************************************************************************
    * @ngdoc method
    * @name submitMeasurementGoal
    * @description
    * Submits a MeasurementGoal, if a state is provided we can set it.
    * This function is reached when a MeasurementGoal is updated with some new
    * value for a specified field, or when it's asked for a state change.
    ********************************************************************************/
    function submitMeasurementGoal(state) {
        if (state != undefined){
         vm.measurementGoalDialog.metadata.state = state;
        }
        else {
            if (vm.measurementGoalDialog.metadata.state == STATES.REJECTED ||
                vm.measurementGoalDialog.metadata.state == STATES.APPROVED)
                vm.measurementGoalDialog.metadata.state = STATES.ONUPDATEWAITING;
        }
 
        if(state == STATES.APPROVED) {
        GraphService.getGraphByMeasurementGoalId(vm.measurementGoalDialog.metadata.id).then(
            function(data) {
                    data.graphs[0].metadata.state = STATES.APPROVED;
                    GraphService.submitGraph(data.graphs[0]).then(
                        function(response) {
                        },
                        function(message) {
                            alert(message);
                        }
                    );
            },
            function(data) {
                    alert('Cannot update associated graph');
            }
        );
        }
 
        if(state == STATES.REJECTED) {
            GraphService.getGraphByMeasurementGoalId(vm.measurementGoalDialog.metadata.id).then(
                function(data) {
                GraphService.deleteGraphById(data.graphs[0].metadata.id).then(
                        function(data) {
                            console.log("Success delete graph");
                        },
                        function(data) {
                            alert('Cannot delete associated graph');
                        }
                );
                },
                function(data) {
                    alert('Cannot get associated graph');
            }

        );
        }
 
        MeasurementGoalService.submitMeasurementGoal(vm.measurementGoalDialog).then(
            function(message) {
                vm.measurementGoalDialog = message.measurementGoals[0];
                $window.alert('Measurement Goal Submitted');
                $location.path('/metricator');
            },
            function(message) {
                alert(message);
            }
    );

    }

    function getMetricsToUpdate() {
    	vm.metrics = MeasurementGoalService.getUpdateMeasurementGoal().metrics;
    }

    function getContextFactorsToUpdate() {
    	vm.contextFactors = MeasurementGoalService.getUpdateMeasurementGoal().contextFactors;
    }

    function getAssumptionsToUpdate() {
    	vm.assumptions = MeasurementGoalService.getUpdateMeasurementGoal().assumptions;
    }

    /********************************************************************************
    * @ngdoc method
    * @name commitChangeState
    * @description
    * This function commits a new state for MeasurementGoal
    ********************************************************************************/
    function commitChangeState(state) {
        switch (state) {
            case STATES.PENDING : 
                _sendForApproval();
                break;
            case STATES.APPROVED : 
                _approve();
                break;
            case STATES.REJECTED : 
                _reject();
                break;
            case STATES.SUSPENDED : 
                _suspend();
                break;
            case STATES.ONUPDATEWAITING :
                if (vm.measurementGoalDialog.metadata.state == STATES.CREATED)
                    _assign();
                else
                    submitMeasurementGoal(STATES.ONUPDATEWAITING);
                break;
            case '' : 
                _addQuestionerIdToMeasurementGoal();
                submitMeasurementGoal(STATES.ONUPDATEWAITING);
                break;
               
        }
    }

    /********************************************************************************
    * @ngdoc method
    * @name _checkToAssign
    * @description
    * Check to Assign metricatorId and questionerId.
    ********************************************************************************/
    function _checkToAssign(){
        UserService.isUser(vm.newMetricatorId).then(
            function(data) {
                UserService.isUser(vm.newQuestionerId).then(
                function(data) {
                        _assign();              
                    },
                    function(data) {
                        alert('This Questioner Doesn\'t exists');        
                    }
                );
            },
            function(data) {
                alert('This Metricator Doesn\'t exists');
            }
        );

    }

    /********************************************************************************
    * @ngdoc method
    * @name _checkToAddQuestioner
    * @description
    * Check if a questionerId is valid.
    ********************************************************************************/
    function _checkToAddQuestioner(){
        UserService.isUser(vm.newQuestionerId).then(
            function(data) {
                _addQuestionerIdToMeasurementGoal();
                submitMeasurementGoal(STATES.ONUPDATEWAITING)
            },
            function(data) {
                alert('This Questioner Doesn\'t exists');        
            }
        );
    }


    /********************************************************************************
    * @ngdoc method
    * @name sendForApproval
    * @description
    * Sends a MeasurementGoal for approval.
    ********************************************************************************/
    function _sendForApproval() {
        submitMeasurementGoal(STATES.PENDING);
    }

    /********************************************************************************
    * @ngdoc method
    * @name reject
    * @description
    * Reject a MeasurementGoal.
    ********************************************************************************/
    function _reject() {
        submitMeasurementGoal(STATES.REJECTED);
    }

    /********************************************************************************
    * @ngdoc method
    * @name reject
    * @description
    * Assign a MeasurementGoal with a metricatorId.
    ********************************************************************************/
    function _assign() {
        
        if (vm.newQuestionerId != undefined){   
            _addQuestionerIdToMeasurementGoal();
        }

        var measurementGoal = {
            userid : null,
            name : null,
            object : '',
            viewPoint : '',
            qualityFocus : '',
            purpose : '',
            OrganizationalGoalId : vm.measurementGoalDialog.OrganizationalGoalId,
            metrics : vm.measurementGoalDialog.metrics,
            questions : vm.measurementGoalDialog.questions,
            pendingQuestions : vm.measurementGoalDialog.pendingQuestions,
            metricatorId : vm.newMetricatorId,
            questionersId : vm.measurementGoalDialog.questionersId,
            contextFactors : vm.measurementGoalDialog.contextFactors,
            assumptions : vm.measurementGoalDialog.assumptions,
            interpretationModel : {
                functionJavascript : '',
                queryNoSQL : ''
            },
            metadata : {
                id : vm.measurementGoalDialog.metadata.id,
                version : vm.measurementGoalDialog.metadata.version,
                tags : [],
                creatorId : vm.measurementGoalDialog.metadata.creatorId,
                state : vm.measurementGoalDialog.metadata.creatorId,
                releaseNote : vm.measurementGoalDialog.metadata.releaseNote,
                entityType : vm.measurementGoalDialog.metadata.entityType,
                versionBus : vm.measurementGoalDialog.metadata.versionBus,
                creationDate : vm.measurementGoalDialog.metadata.creationDate,
                lastVersionDate : vm.measurementGoalDialog.metadata.lastVersionDate
            }
        };
        vm.measurementGoalDialog = measurementGoal;
        submitMeasurementGoal(STATES.ONUPDATEWAITING);    
    }

    /********************************************************************************
    * @ngdoc method
    * @name approve
    * @description
    * Approve a MeasurementGoal.
    ********************************************************************************/
    function _approve() {
        submitMeasurementGoal(STATES.APPROVED);    
    }

    /********************************************************************************
    * @ngdoc method
    * @name suspend
    * @description
    * Suspend a MeasurementGoal.
    ********************************************************************************/
    function _suspend() {
        submitMeasurementGoal(STATES.SUSPENDED);
    }

    /********************************************************************************
    * @ngdoc method
    * @name getApprovedMetrics
    * @description
    * Get approved metrics.
    ********************************************************************************/
    function getApprovedMetrics(){

    	if (vm.externalMetricDialog.length === 0){
	        MetricService.getAllApproved().then(
	            function(data) {
	                console.log('SUCCESS GET APPROVED METRICS');
	               // console.log(data.metricsDTO);
	                //vm.externalMetricDialog = data.metricsDTO;
	                MetricService.storeExternalMetric(data.metricsDTO);
    			    MetricModalService.openExternalMetricModal();
	            },
	            function(data) {
	                alert('Error retriving Metrics');
	            }
	        );
     	}
     	else {
     		MetricModalService.openExternalMetricModal();
     	}
    }

    /********************************************************************************
    * @ngdoc method
    * @name getExternalContextFactors
    * @description
    * Get external context factors.
    ********************************************************************************/
    function getExternalContextFactors(){

        if (vm.externalContextFactorDialog.length === 0){
            MeasurementGoalService.getExternalContextFactors().then(
                function(data) {
                    console.log('SUCCESS GET EXTERNAL CONTEXT FACTORS');
                  //  console.log(data);
                    //vm.externalContextFactorDialog = data;
                    ContextFactorService.storeExternalContextFactor(data);
    			    ContextFactorModalService.openExternalContextFactorModal();
                },
                function(data) {
                    alert('Error retrieving Context Factors');
                }
            );
        }
        else {
            ContextFactorModalService.openExternalContextFactorModal();
        }
    }

    /********************************************************************************
    * @ngdoc method
    * @name getExternalAssumptions
    * @description
    * Get external assumptions.
    ********************************************************************************/
    function getExternalAssumptions(){
        if (vm.externalAssumptionDialog.length === 0){
            MeasurementGoalService.getExternalAssumptions().then(
                function(data) {
                    console.log('SUCCESS GET EXTERNAL ASSUMPTIONS');
                  //  console.log(data);
                    //vm.externalAssumptionDialog = data;
                    AssumptionService.storeExternalAssumption(data);
    			    AssumptionModalService.openExternalAssumptionModal();
                },
                function(data) {
                    alert('Error retrieving Assumptions');
                }
            );
        }
        else {
            AssumptionModalService.openExternalAssumptionModal();
        }
    }

    function setQuestionDialog(questionToAssignId){
        QuestionStorageFactory.setQuestion(vm.questions[questionToAssignId]);
        QuestionStorageFactory.setMeasurementGoal(vm.measurementGoalDialog);
        QuestionModalFactory.openQuestionModal();
    }
 
    function setPQuestionDialog(questionToAssignId){
        QuestionStorageFactory.setQuestion(vm.pendingQuestions[questionToAssignId]);
        QuestionStorageFactory.setMeasurementGoal(vm.measurementGoalDialog);
        QuestionModalFactory.openQuestionModal();
    }

    function setMetricDialog(metricToAssignId){
            MetricService.storeMetric(vm.metrics[metricToAssignId]);
            MetricModalService.openMetricModal();
    }

    function setContextFactorDialog(contextFactorToAssignId){
            ContextFactorService.storeContextFactor(vm.contextFactors[contextFactorToAssignId]);
            ContextFactorModalService.openContextFactorModal();
    }

    function setAssumptionDialog(assumptionToAssignId){
            AssumptionService.storeAssumption(vm.assumptions[assumptionToAssignId]);
            AssumptionModalService.openAssumptionModal();
    }

    function goToUpdateMeasurementGoal(){    
        $location.path('/measurementgoal');
    }

    /********************************************************************************
    * @ngdoc method
    * @name addTagToMeasurementGoal
    * @description
    * Add tag to a measurement goal.
    ********************************************************************************/
    function addTagToMeasurementGoal(){
        if (vm.measurementGoalDialog.metadata.tags === null) vm.measurementGoalDialog.metadata.tags = [];
        vm.measurementGoalDialog.metadata.tags.push(vm.newTag);
    }

    /********************************************************************************
    * @ngdoc method
    * @name addTagToMeasurementGoal
    * @description
    * Remove tag from a measurement goal.
    ********************************************************************************/
    function removeTagFromMeasurementGoal(index){
        vm.measurementGoalDialog.metadata.tags.splice(index, 1);
    }

    /********************************************************************************
    * @ngdoc method
    * @name removeSomethingFromMeasurementGoal
    * @description
    * Remove something from measurement goal.
    ********************************************************************************/
    function removeSomethingFromMeasurementGoal(typeObject, index){
        
        switch(typeObject) {
            case 'Metric':
                vm.measurementGoalDialog.metrics.splice(index, 1);
                vm.metrics.splice(index, 1);
                break;
            case 'Question':
                vm.measurementGoalDialog.questions.splice(index, 1);
                vm.questions.splice(index, 1);
                break;
            case 'ContextFactor':
                vm.measurementGoalDialog.contextFactors.splice(index, 1);
                vm.contextFactors.splice(index, 1);
                break;
            case 'Assumption':
                vm.measurementGoalDialog.assumptions.splice(index, 1);
                vm.assumptions.splice(index, 1);
                break;
        }

        $window.alert('Item removed');
        console.log("MEASUREMENT GOAL AFTER REMOVE");
        console.log(vm.measurementGoalDialog);
    }

    /********************************************************************************
    * @ngdoc method
    * @name isModifiable
    * @description
    * Measurement Goal can be updated.
    ********************************************************************************/
    function isModifiable(){
        return StateMachineMeasurementGoalService.isModifiable();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isForApproval
    * @description
    * Measurement Goal can be sent for approval.
    ********************************************************************************/ 
    function isForApproval(){
        return StateMachineMeasurementGoalService.isForApproval();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToReject
    * @description
    * Measurement Goal can be rejected.
    ********************************************************************************/ 
    function isToReject(){
        return StateMachineMeasurementGoalService.isToReject();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToApproved
    * @description
    * Measurement Goal can be approved.
    ********************************************************************************/ 
    function isToApprove(){
        return StateMachineMeasurementGoalService.isToApprove();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToChangeRequest
    * @description
    * Measurement Goal can be affected by change request.
    ********************************************************************************/ 
    function isToChangeRequest(){
        return StateMachineMeasurementGoalService.isToChangeRequest();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAssign
    * @description
    * Measurement Goal can be assigned.
    ********************************************************************************/ 
    function isToAssign(){
        return StateMachineMeasurementGoalService.isToAssign();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAddQuestioner
    * @description
    * Can add a Questioner to a Measurement Goal.
    ********************************************************************************/ 
    function isToAddQuestioner(){
        return StateMachineMeasurementGoalService.isToAddQuestioner();
    }

    /********************************************************************************
    * @ngdoc method
    * @name noMetricatorsForMG
    * @description
    * This function checks if a Measurement Goal contains a metricatorId.
    ********************************************************************************/ 
    function noMetricatorsForMG(){
        return StateMachineMeasurementGoalService.noMetricatorsForMG();
    }

    /********************************************************************************
    * @ngdoc method
    * @name noQuestionersForMG
    * @description
    * This function checks if a Measurement Goal contains a questionerId.
    ********************************************************************************/ 
    function noQuestionersForMG(){   
        return StateMachineMeasurementGoalService.noQuestionersForMG();
    }

    /********************************************************************************
    * @ngdoc method
    * @name addQuestionerIdToMeasurementGoal
    * @description
    * This function adds a questionerId to a Measurement Goal. Note that
    * more than one questionerId can be in a Measurement Goal.
    ********************************************************************************/ 
    function _addQuestionerIdToMeasurementGoal(){
        vm.measurementGoalDialog.questionersId.push(vm.newQuestionerId);
    }

    /********************************************************************************
    * @ngdoc method
    * @name changeState
    * @description
    * Pass to a new page for changing the state of a MeasurementGoal and fill a 
    * release note.
    ********************************************************************************/ 
    function changeState(){
        $location.path('/measurementgoalchangestate');
    }

    /********************************************************************************
    * @ngdoc method
    * @name _init
    * @description
    * Initializes the controller.
    ********************************************************************************/
    function _init() {
        vm.loading = false;
    }

}

})();
