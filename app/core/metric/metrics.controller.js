(function() {  'use strict';

/************************************************************************************
* @ngdoc controller
* @name MetricsController
* @module metricapp
* @requires $scope
* @requires $location
* @requires $filter
* @requires MetricService
*
* @description
* Realizes the control layer for `metrics.view`.
************************************************************************************/

angular.module('metricapp')

.controller('MetricsController', MetricsController);

MetricsController.$inject = ['$scope', '$location', '$filter', 'MetricService', 'AssumptionService', 'AssumptionModalService', 'ContextFactorService', 'ContextFactorModalService', 'MeasurementGoalService', 'AuthService'];

function MetricsController($scope, $location, $filter, MetricService, AssumptionService, AssumptionModalService, ContextFactorService, ContextFactorModalService, MeasurementGoalService, AuthService) {

    var vm = this;

    vm.loadMore = loadMore;
    vm.search = search;
    vm.reset=reset;
    vm.update=update;
    vm.create=create;
    vm.openModal=openModal;
    vm.setAssumptionDialog=setAssumptionDialog;
    vm.setContextFactorDialog=setContextFactorDialog;
    vm.goToRead=goToRead;
    vm.getLabelState = getLabelState;
    vm.openTab=openTab;
    vm.contextFactors = [];
    vm.assumptions = [];

    vm.labelsForState = [
       {state:'OnUpdate',label: "label label-primary label-form"},
       {state:'Pending',label: "label label-default label-form"},
       {state:'Created',label: "label label-default label-form"},
       {state:'Approved',label: "label label-warning label-form"},
       {state:'Rejected',label: "label label-danger label-form"}];

    _init();
 
 function openTab(evt, name) {
 // Declare all variables
 var i, tabcontent, tablinks;
 
 // Get all elements with class="tabcontent" and hide them
 tabcontent = document.getElementsByClassName("tabcontent");
 for (i = 0; i < tabcontent.length; i++) {
 tabcontent[i].style.display = "none";
 }
 
 // Get all elements with class="tablinks" and remove the class "active"
 tablinks = document.getElementsByClassName("tablinks");
 for (i = 0; i < tablinks.length; i++) {
 tablinks[i].className = tablinks[i].className.replace(" active", "");
 }
 
 // Show the current tab, and add an "active" class to the link that opened the tab
 document.getElementById(name).style.display = "block";
 document.getElementById("tablink " + name).className += " active";
 }
 
 function setAssumptionDialog(assumptionToAssignId){
    AssumptionService.storeAssumption(vm.assumptions[assumptionToAssignId]);
    AssumptionModalService.openAssumptionModal();
 }
 
 function setContextFactorDialog(contextFactorToAssignId){
    ContextFactorService.storeContextFactor(vm.contextFactors[contextFactorToAssignId]);
    ContextFactorModalService.openContextFactorModal();
 }
 


    function loadMore() {
        if (vm.idx < vm.buffer.length) {
            var e = Math.min(vm.idx + vm.step, vm.buffer.length);
            vm.metrics = vm.metrics.concat(vm.buffer.slice(vm.idx, e));
            vm.idx = e;
        }
    }

    function goToRead(id){
      console.log('go to '+'#/metrics/'+id);
      $location.path('#/metrics/'+id);
   }

    function search(query) {
        vm.buffer = $filter('orderBy')($filter('filter')(vm.data, query), vm.orderBy,true);
    }

    function reset(){
      _init();
   }


       function openModal(metric){
         vm.metricDialog=metric;
         vm.modal=true;
      }

   function _load(){
      if(vm.mine){
         if(!vm.approved){
         _loadMyMetrics();
      }else{
         _loadMyApprovedMetrics();
      }
      }
      else{
         if(!vm.approved){
         _loadAllMetrics();
      }else{
         _loadAllApprovedMetrics();
      }
      }
   }

    function _loadAllMetrics() {
        vm.loading = true;
        vm.success = false;
        MetricService.getAll().then(
            function(resolve) {
                vm.data = angular.copy(resolve.metricsDTO);
                vm.buffer = $filter('orderBy')(vm.data, vm.orderBy,true);
                vm.success = true;
            },
            function(reject) {
                vm.errmsg = reject.errmsg;
                vm.success = false;
            }
        ).finally(function() {
            vm.loading = false;}
        );
  }

  function _loadMyMetrics() {
     vm.loading = true;
     vm.success = false;
     MetricService.getAllMine().then(
          function(resolve) {
              vm.data = angular.copy(resolve.metricsDTO);
              vm.buffer = $filter('orderBy')(vm.data, vm.orderBy,true);
              vm.success = true;
          },
          function(reject) {
              vm.errmsg = reject.errmsg;
              vm.success = false;
          }
     ).finally(function() {
          vm.loading = false;
     });
}

function _loadAllApprovedMetrics() {
    vm.loading = true;
    vm.success = false;
    MetricService.getAllApproved().then(
        function(resolve) {
            vm.data = [];
            vm.data = angular.copy(resolve.metricsDTO);
            vm.buffer = $filter('orderBy')(vm.data, vm.orderBy,true);
            vm.success = true;
        },
        function(reject) {
            vm.errmsg = reject.errmsg;
            vm.success = false;
        }
    ).finally(function() {
        vm.loading = false;
    });
}

function _loadMyApprovedMetrics() {
 vm.loading = true;
 vm.success = false;
 MetricService.getAllApproved().then(
      function(resolve) {
         vm.data = [];
          var temp = angular.copy(resolve.metricsDTO);
          for (var m=0; m<temp.length; m++){
             if (temp[m].metricatorId == vm.user){
                vm.data.push(angular.copy(temp[m]));
             }
          }
          vm.buffer = $filter('orderBy')(vm.data, vm.orderBy,true);
          vm.success = true;
      },
      function(reject) {
          vm.errmsg = reject.errmsg;
          vm.success = false;
      }
 ).finally(function() {
      vm.loading = false;
 });
}

function create() {
 MetricService.create().then(
      function(resolve) {

      },
      function(reject) {

      }
 ).finally(function() {
      vm.update();
 });
}


  function update(){
     _load();
     vm.search(vm.query);
 }

    function _init() {
 
        MeasurementGoalService.getExternalContextFactors().then(
                function(data) {
                    console.log('SUCCESS GET EXTERNAL CONTEXT FACTORS');
                    vm.contextFactors = data;
                                                                
                    MeasurementGoalService.getExternalAssumptions().then(
                        function(data) {
                            console.log('SUCCESS GET EXTERNAL ASSUMPTIONS');
                            vm.assumptions = data;
                            _initVm();
                        });
                });
    }
 
    function _initVm() {
 
        vm.userId = AuthService.getUser().username;
        vm.user = AuthService.getUser().username;

        vm.role = AuthService.getUser().role;
        vm.metricator=false;
        console.log(vm.role);
        if(vm.role == 'Metricator'){
           vm.metricator=true;
        }
        vm.loading = true;
        vm.success = true;
        vm.errmsg = null;
        vm.data = [];
        vm.buffer = [];
        vm.metrics = [];
        vm.mine=false;
        vm.modal=false;
        vm.idx = 0;
        vm.step = 9;
        vm.query = '';
        vm.approved=false;
        vm.orderBy = 'metadata.lastVersionDate';
        if(vm.role=='Metricator'){
           vm.mine=true;
        }
        _load();
        $scope.$watch('vm.buffer', function() {
            vm.idx = 0;
            var e = Math.min(vm.idx + vm.step, vm.buffer.length);
            vm.metrics = vm.buffer.slice(vm.idx, e);
            vm.idx = e;
        });
 
    }

    function getLabelState(state){
      return $filter('filter')(vm.labelsForState, function (d) {return d.state == state;})[0].label;
    }
 
 
}

})();
