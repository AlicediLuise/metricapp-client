(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionUpdateCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller("QuestionUpdateCtrl", QuestionUpdateCtrl);

QuestionUpdateCtrl.$inject = ['$window', '$location', 'QuestionStorageFactory', 'QuestionsCrudFactory', 'QuestionModalFactory'];

function QuestionUpdateCtrl($window, $location, QuestionStorageFactory, QuestionsCrudFactory, QuestionModalFactory){

    var ctrl = this;
    
    ctrl.question = angular.copy(QuestionStorageFactory.getQuestion());
    ctrl.isCommitted = false;

    console.log("QUESTION");
   // console.log(ctrl.question);

    ctrl.reset = function(){
        ctrl.question = angular.copy(QuestionStorageFactory.getQuestion());   
    };

    ctrl.updateQuestion = function(){

        if(ctrl.question.metadata.state == QuestionsCrudFactory.STATES.CREATED){
            ctrl.question.metadata.state = QuestionsCrudFactory.STATES.ONUPDATE;
        }
        else if(ctrl.question.metadata.state == QuestionsCrudFactory.STATES.REJECTED){
            ctrl.question.metadata.state = QuestionsCrudFactory.STATES.ONUPDATE;
        }
        
        QuestionsCrudFactory.update(ctrl.question).then(
            function(data, status){

                if(!data.data.error){
                    QuestionStorageFactory.setQuestion(data.data.questionList[0]);
                    ctrl.question = angular.copy(QuestionStorageFactory.getQuestion());
                    QuestionModalFactory.openQuestionModal();
                    ctrl.isCommitted = true;
                }
                else{
                    $window.alert("Error: " + data.data.error);
                }
            },
            
            function(data, status){
                $window.alert("Error: status " + status);
            }
        );
    };

    // ctrl.finalizeQuestion = function(){

    //     if(ctrl.question.metadata.state == QuestionsCrudFactory.STATES.CREATED || !ctrl.isCommitted){
    //         $window.alert("You have to commit the update first!");
    //         return;
    //     }
    //     else if(ctrl.question.metadata.state == QuestionsCrudFactory.STATES.ONUPDATE){
    //         ctrl.question.metadata.state = QuestionsCrudFactory.STATES.PENDING;
    //     }
        
    //     QuestionsCrudFactory.update(ctrl.question).then(
    //         function(data, status){
    //             $window.alert("Question Finalized");
    //             $location.path('/dashboardQuestioner');
    //         },
            
    //         function(data, status){
    //             $window.alert("Error: status " + status);
    //         }
    //     );
    // };

    ctrl.getTags = function(){
        return ctrl.question.metadata.tags.join();
    };

    ctrl.addTagToQuestion = function() {
        ctrl.question.metadata.tags.push(ctrl.newTag);
    };

    ctrl.removeTagFromQuestion = function(index){
        ctrl.question.metadata.tags.splice(index, 1);
    };

    ctrl.getStates = function(){
        return QuestionsCrudFactory.STATES;
    };
}

})();
