(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionSearchCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller('QuestionSearchCtrl', QuestionSearchCtrl);

QuestionSearchCtrl.$inject = ['$location', '$window', 'QuestionsCrudFactory', 'QuestionStorageFactory', 'QuestionModalFactory'];

function QuestionSearchCtrl($location, $window, QuestionsCrudFactory, QuestionStorageFactory, QuestionModalFactory) {

        var ctrl = this;

        // QuestionsCrudFactory.getAll().then(
        //     function(data, status){
        //         ctrl.questions = data.data.questionList;
        //     },
            
        //     function(data, status){
        //         $window.alert(data.data.error);
        //         ctrl.questions = null;
        //     }
        // );
        

        ctrl.searchBy = function(keyword, searchField){
            QuestionsCrudFactory.get([keyword], [searchField]).then(
                function(data, status){
                    if(!data.data.error){
                        ctrl.questions = data.data.questionList;
                    }
                    else{
                        $window.alert("Error: " + data.data.error);
                    }
                },

                function(data, status){
                    $window.alert(status);
                }
            );
        };

        ctrl.open = function (index) {

            QuestionStorageFactory.setQuestion(ctrl.questions[index]);
            QuestionModalFactory.openQuestionModal();

        };

    }

})();
