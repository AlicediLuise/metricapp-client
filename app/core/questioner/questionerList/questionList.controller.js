(function() {'use strict';

angular.module('metricapp')

.controller('QuestionListCtrl', QuestionListCtrl);

QuestionListCtrl.$inject = ['AuthService', '$location', 'QuestionsCrudFactory',
 'QuestionStorageFactory', 'QuestionModalFactory', '$window'];

function QuestionListCtrl(AuthService, $location, QuestionsCrudFactory,
     QuestionStorageFactory, QuestionModalFactory, $window) {

    var ctrl = this;

    ctrl.getQuestions = getQuestions;
    ctrl.getAllQuestions = getAllQuestions;
    ctrl.questions = [];

    ctrl.questionDialog = null;

    if(AuthService.getUser().role == "Questioner"){
        ctrl.getQuestions();
    }
    else{
        ctrl.getAllQuestions();
    }

    _init();

    /********************************************************************************
    * @ngdoc method
    * @name submitMeasurementGoal
    * @description
    * Get active metrics for a metricator.
    ********************************************************************************/
    function getQuestions(){
         QuestionsCrudFactory.get([QuestionStorageFactory.getQuestioner().username], ["questionerId"]).then(
            function(data) {
                if(!data.data.error){
                    ctrl.questions = data.data.questionList;
                }
                else{
                    $window.alert("Error: " + data.data.error);
                }
            },
            function(data) {
                alert('Error retrieving Questions');
            }
        );
    }

    function getAllQuestions(){
        QuestionsCrudFactory.getAll().then(
            function(data){
                if(!data.data.error){
                    ctrl.questions = data.data.questionList;
                }
                else{
                    $window.alert("Error: " + data.data.error);
                }
            },
            function(data){
                alert('Error retrieving Questions');
            }
        );
    }

    // function countOnUpdate(){
    //     QuestionsCrudFactory.count(QuestionsCrudFactory.STATES.ONUPDATE, QuestionStorageFactory.getQuestioner().username).then(
    //         function(data) {
    //             ctrl.count = data.data.count;
    //             $window.alert(ctrl.count);
    //         },
    //         function(data) {
    //             alert('Error retrieving Questions');
    //         }
    //     );
    // }

    ctrl.open = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.questions[index]);
        QuestionModalFactory.openQuestionModal();
    };

    function _init(){
        //
    }

}

})();
