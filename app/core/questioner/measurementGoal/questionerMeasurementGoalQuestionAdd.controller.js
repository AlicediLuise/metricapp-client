(function() {'use strict';

angular.module('metricapp')

.controller('QuestionMeasurementGoalAddCtrl', QuestionMeasurementGoalAddCtrl);

QuestionMeasurementGoalAddCtrl.$inject = ['$location', 'QuestionsCrudFactory', 'MeasurementGoalService', 'QuestionerMeasurementGoalModalFactory',
 'QuestionStorageFactory', 'QuestionModalFactory', '$window'];

function QuestionMeasurementGoalAddCtrl($location, QuestionsCrudFactory, MeasurementGoalService, QuestionerMeasurementGoalModalFactory,
     QuestionStorageFactory, QuestionModalFactory, $window) {

    var ctrl = this;

    ctrl.newQuestions = [];
    ctrl.questions = [];
    ctrl.pendingQuestions = [];
    ctrl.getNewQuestions = getNewQuestions;
    ctrl.getQuestionAndPendingQuestions = getQuestionAndPendingQuestions;

    ctrl.mgDialog = QuestionStorageFactory.getMeasurementGoal();
    
    _init();
 

    function getNewQuestions() {
         
     	QuestionsCrudFactory.getFromBus().then(
     		function(data){
                if(!data.data.error){
                    ctrl.newQuestions = data.data.questionList;
                }
                else{
                    $window.alert("Error: " + data.data.error);
                }
     		},
     		function(data){
     			$window.alert("Error retrieving questions from bus");
     		}
 		);

    };
    
    ctrl.newOpen = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.newQuestions[index]);
        QuestionModalFactory.openQuestionModal();
    };
 
    ctrl.qOpen = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.questions[index]);
        QuestionModalFactory.openQuestionModal();
    };

    ctrl.pqOpen = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.pendingQuestions[index]);
        QuestionModalFactory.openQuestionModal();
    };


    ctrl.openMg = function(){
        QuestionStorageFactory.setMeasurementGoal(ctrl.mgDialog);
        QuestionerMeasurementGoalModalFactory.openMeasurementGoalModal();
    };

    ctrl.addToMeasurementGoal = function(index){

    	var i;
    	for(i=0; i<ctrl.mgDialog.questions.length; i++){
    		if(ctrl.newQuestions[index].metadata.id == ctrl.mgDialog.questions[i].instance){
    			$window.alert("The question is already present in the Measurement Goal");
    			return; 
    		}
    	}

        var questionInstance = {
            busVersion : "",
            instance : ctrl.newQuestions[index].metadata.id,
            objIdLocalToPhase : "",
            tags: ctrl.newQuestions[index].metadata.tags,
            typeObj: "Question"
        };

        ctrl.mgDialog.questions.push(questionInstance);
        MeasurementGoalService.submitMeasurementGoal(ctrl.mgDialog).then(
            function(data){
                $window.alert("Question " + questionInstance.instance + " added to the Measurement Goal");
                ctrl.mgDialog.metadata.version++;
            },
            
            function(data){
                $window.alert("Error adding question");
            }
        );

        
    };
 
    ctrl.removeQFromMeasurementGoal = function(index){
 
        if(ctrl.mgDialog.questions.length == 0) {
            $window.alert("You already remove this question");
            return;
        }
 
        var i;
        for(i=0; i<ctrl.mgDialog.questions.length; i++){
            if(ctrl.questions[index].metadata.id == ctrl.mgDialog.questions[i].instance){
                ctrl.mgDialog.questions.splice(i, 1);
                MeasurementGoalService.submitMeasurementGoal(ctrl.mgDialog).then(
                    function(data){
                        $window.alert("Question " + ctrl.questions[index].metadata.id + " removed from the Measurement Goal");
                        ctrl.mgDialog.metadata.version++;
                    },
                                                                  
                    function(data){
                        $window.alert("Error removing question");
                    }
                );
                return;
            }
        }
        $window.alert("You already remove this question");
        return;
    };
 
 
    ctrl.removePQFromMeasurementGoal = function(index){
 
        if(ctrl.mgDialog.pendingQuestions.length == 0) {
            $window.alert("You already remove this question");
            return;
        }
 
        var i;
        for(i=0; i<ctrl.mgDialog.pendingQuestions.length; i++){
            if(ctrl.pendingQuestions[index].metadata.id == ctrl.mgDialog.pendingQuestions[i].instance){
                ctrl.mgDialog.pendingQuestions.splice(i, 1);
                MeasurementGoalService.submitMeasurementGoal(ctrl.mgDialog).then(
                    function(data){
                        $window.alert("Question " + ctrl.pendingQuestions[index].metadata.id + " removed from the Measurement Goal");
                        ctrl.mgDialog.metadata.version++;
                    },
                                                                  
                    function(data){
                        $window.alert("Error removing question");
                    }
                );
                return;
            }
        }
            $window.alert("You already remove this question");
            return;
    };

 
    function getQuestionAndPendingQuestions() {
        MeasurementGoalService.getMeasurementGoalExternals(ctrl.mgDialog.metadata.id).then(
            function(data){
                ctrl.pendingQuestions = data.pendingQuestions;
                ctrl.questions = data.questions;
                                                                                           
                for(var i=0; i<ctrl.questions.length; i++)
                    for(var j=0; j<ctrl.newQuestions.length; j++)
                        if(ctrl.questions[i].metadata.id == ctrl.newQuestions[j].metadata.id) {
                                    ctrl.newQuestions.splice(j, 1);
                                    break;
                        }
                                                                                           
                for(var i=0; i<ctrl.pendingQuestions.length; i++)
                    for(var j=0; j<ctrl.newQuestions.length; j++)
                        if(ctrl.pendingQuestions[i].metadata.id == ctrl.newQuestions[j].metadata.id) {
                                    ctrl.newQuestions.splice(j, 1);
                                    break;
                        }
            },
                                                                                    
            function(data){
                $window.alert("Error in retrieving wrong questions");
            }
        );
    };



    function _init(){
        ctrl.getNewQuestions();
        ctrl.getQuestionAndPendingQuestions();
    }

}

})();
