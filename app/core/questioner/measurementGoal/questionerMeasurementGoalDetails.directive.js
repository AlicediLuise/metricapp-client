(function () {
  'use strict';

  angular.module('metricapp')
      .directive('questionerMg', questionerMg);

  function questionerMg() {
    return {
      restrict: 'E',
      templateUrl: 'dist/views/questioner/measurementGoal/measurementGoalDetails.view.html'
    };
  }

})();
