(function() {'use strict';

angular.module('metricapp')

.controller('QuestionMeasurementGoalListCtrl', QuestionMeasurementGoalListCtrl);

QuestionMeasurementGoalListCtrl.$inject = ['$location', 'QuestionsCrudFactory', 'QuestionerMeasurementGoalModalFactory',
 'QuestionStorageFactory', 'QuestionModalFactory', '$window', 'QuestionerMetricModalFactory', 'MeasurementGoalService'];

function QuestionMeasurementGoalListCtrl($location, QuestionsCrudFactory, QuestionerMeasurementGoalModalFactory,
     QuestionStorageFactory, QuestionModalFactory, $window, QuestionerMetricModalFactory, MeasurementGoalService) {

    var ctrl = this;

    ctrl.questions = [];
    ctrl.metrics = [];

    ctrl.mgDialog = QuestionStorageFactory.getMeasurementGoal();
    
    _init();
    /********************************************************************************
    * @ngdoc method
    * @name submitMeasurementGoal
    * @description
    * Get active metrics for a metricator.
    ********************************************************************************/
    
    ctrl.openQuestion = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.questions[index]);
        QuestionModalFactory.openQuestionModal();
    };
 
    ctrl.openPendingQuestion = function (index) {
        QuestionStorageFactory.setQuestion(ctrl.pendingQuestions[index]);
        QuestionModalFactory.openQuestionModal();
    };

    ctrl.openMetric = function(index){
        QuestionStorageFactory.setMetric(ctrl.metrics[index]);
        QuestionerMetricModalFactory.openMetricModal();  
    };

    ctrl.openMg = function(){
        QuestionStorageFactory.setMeasurementGoal(ctrl.mgDialog);
        QuestionerMeasurementGoalModalFactory.openMeasurementGoalModal();
    };

    ctrl.getMeasurementGoal = function(){
        MeasurementGoalService.getMeasurementGoalExternals(ctrl.mgDialog.metadata.id).then(
            function(data, status){
                ctrl.questions = data.questions;
                ctrl.pendingQuestions = data.pendingQuestions;
                ctrl.metrics = data.metrics;
            },

            function(data, status){
                $window.alert("Error in retrieving measurement goal data");
            }
        );
        
    };

    function _init(){
        //
    }

    this.getMeasurementGoal();
}

})();
