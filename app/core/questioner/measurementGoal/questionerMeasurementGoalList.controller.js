(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionerMeasurementGoalListCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller('QuestionerMeasurementGoalListCtrl', QuestionerMeasurementGoalListCtrl);

QuestionerMeasurementGoalListCtrl.$inject = ['$location', '$window', 'MeasurementGoalService', 
	'QuestionStorageFactory', 'QuestionerMeasurementGoalModalFactory'];

function QuestionerMeasurementGoalListCtrl($location, $window, MeasurementGoalService, QuestionStorageFactory, 
    QuestionerMeasurementGoalModalFactory) {

    var ctrl = this;

    ctrl.getMeasurementGoals = function(){
       MeasurementGoalService.getMeasurementGoalsBy(QuestionStorageFactory.getQuestioner().username, "questionerId").then(
            function(data) {
                if(!data.error){
                    ctrl.measurementGoals = data.measurementGoals;
                }
                else{
                    $window.alert("Error: " + data.error);
                }
            },
            function(data) {
                alert('Error retrieving Measurement Goals');
            }
        );
    };

    ctrl.open = function (index) {
        QuestionStorageFactory.setMeasurementGoal(ctrl.measurementGoals[index]);
        QuestionerMeasurementGoalModalFactory.openMeasurementGoalModal();
    };

	this.getMeasurementGoals();
    

}

})();
