(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionDetailsCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller('QuestionDetailsCtrl', QuestionDetailsCtrl);

QuestionDetailsCtrl.$inject = ['AuthService', '$location', '$window', 'QuestionStorageFactory', 'QuestionsCrudFactory', '$routeParams'];

function QuestionDetailsCtrl(AuthService, $location, $window, QuestionStorageFactory, QuestionsCrudFactory, $routeParams) {

        var ctrl = this;
        ctrl.questionDialog = {};

        ctrl.isExpert = AuthService.getUser().role == 'GQMExpert';

        $window.alert(AuthService.getUser().role);
        $window.alert(AuthService.getUser().role == 'GQMExpert');

        QuestionsCrudFactory.get([$routeParams.questionId], ["id"]).then(
        		function(data){
        		//	console.log(data);
        			ctrl.questionDialog = data.data.questionList[0];
        		},

        		function(data){
        			$window.alert("Error retrieving question");
        		}
        	);

        ctrl.showReleaseNote = false;

        ctrl.deleteQuestion = function (questionId) {
            $uibModalInstance.dismiss("closing");
            QuestionsCrudFactory.delete(questionId).then(
                function(data, status){
                    $window.alert("deleted question " + questionId);
                },

                function(data, status){
                    $window.alert(status);
                }
            );

        };

        ctrl.rejectQuestion = function(){
            ctrl.showReleaseNote = true;
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.PENDING){

                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.REJECTED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.approveQuestion = function(){
            ctrl.showReleaseNote = true;
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.PENDING){

                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.APPROVED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.suspendQuestion = function(){
            ctrl.showReleaseNote = true;
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.REJECTED){

                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.SUSPENDED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.notifyQuestion = function(){

            $window.alert("notifying question");
            $uibModalInstance.dismiss("closing");

            QuestionsCrudFactory.update(ctrl.questionDialog).then(
                function(data, status){
                    $window.alert("Question elaborated");
                },

                function(data, status){
                    $window.alert("Error: status " + status);
                }
            );

        }

        ctrl.closeModal = function(){
            $uibModalInstance.dismiss("closing");
        };


    }

})();
