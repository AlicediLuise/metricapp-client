(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionModalCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller('QuestionModalCtrl', QuestionModalCtrl);

QuestionModalCtrl.$inject = ['AuthService', '$location', '$uibModalInstance', '$window', 'QuestionsCrudFactory', 'QuestionStorageFactory'];

function QuestionModalCtrl(AuthService, $location, $uibModalInstance, $window, QuestionsCrudFactory, QuestionStorageFactory) {

        var ctrl = this;

        ctrl.questionDialog = QuestionStorageFactory.getQuestion();
        ctrl.measurementGoal = QuestionStorageFactory.getMeasurementGoal();



        ctrl.isQuestioner = AuthService.getUser().role == 'Questioner';
        ctrl.isMetricator = AuthService.getUser().role == 'Metricator';
        ctrl.isExpert = AuthService.getUser().role == 'GQMExpert';

        ctrl.showReleaseNote = false;
 
        ctrl.isNoteModifiable = function() {
                var mg = ctrl.measurementGoal;
                if(ctrl.isMetricator && (mg.metadata.state == 'OnUpdateQuestionerEndpoint' ||
                    mg.metadata.state == 'OnUpdateWaitingQuestions' || mg.metadata.state == 'Rejected' )) {
                        for(var i=0; i < mg.pendingQuestions.length; i++)
                            if(mg.pendingQuestions[i].instance == ctrl.questionDialog.metadata.id)
                                return true;
                        return false;
                }
                return true;
        }
 
        ctrl.notifyWrongQuestion = function() {
                var mg = ctrl.measurementGoal;
                mg.metadata.state = QuestionsCrudFactory.STATES.ONUPDATEWAITING;
                var index = -1;
 
                for(var i=0; i < mg.questions.length; i++)
                    if(mg.questions[i].instance == ctrl.questionDialog.metadata.id) {
                        index = i;
                        break;
                    }
 
                if(index != -1) {
                    mg.pendingQuestions.push(mg.questions[index]);
                    mg.questions.splice(index, 1);
                    $window.alert("The question is notified as wrong");
                }
                else{
                    $window.alert("You already notify this question as wrong");
                }
        }


        ctrl.deleteQuestion = function (questionId) {
            $uibModalInstance.dismiss("closing");
            QuestionsCrudFactory.delete(questionId).then(
                function(data, status){
                    $window.alert("deleted question " + questionId);
                },

                function(data, status){
                    $window.alert(status);
                }
            );

        };

        ctrl.rejectQuestion = function(){
            
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.PENDING){
                ctrl.showReleaseNote = true;
                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.REJECTED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.approveQuestion = function(){
            
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.PENDING){
                ctrl.showReleaseNote = true;
                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.APPROVED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.changeQuestion = function(){
            
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.APPROVED){
                ctrl.showReleaseNote = true;
                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.ONUPDATE;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.suspendQuestion = function(){
            
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.REJECTED){
                ctrl.showReleaseNote = true;
                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.SUSPENDED;

            }
            else{
                $window.alert("Invalid question state");
            }

        }

        ctrl.notifyQuestion = function(){

            $window.alert("notifying question");
            $uibModalInstance.dismiss("closing");

            QuestionsCrudFactory.update(ctrl.questionDialog).then(
                function(data, status){
                    $window.alert("Question elaborated");
                },

                function(data, status){
                    $window.alert("Error: status " + status);
                }
            );

        }

        ctrl.closeModal = function(){
            $uibModalInstance.dismiss("closing");
        };

        ctrl.editQuestion = function(){
            $uibModalInstance.dismiss("closing");
            QuestionStorageFactory.setQuestion(ctrl.questionDialog);
            $location.path('/questionUpdate');
        };

        ctrl.sendForApproval = function(){

            $uibModalInstance.dismiss("closing");
            if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.ONUPDATE){

                ctrl.questionDialog.metadata.state = QuestionsCrudFactory.STATES.PENDING;

                QuestionsCrudFactory.update(ctrl.questionDialog).then(
                    function(data, status){
                        $window.alert("Question sent for approval");
                        $location.path('/questionList');
                    },

                    function(data, status){
                        $window.alert("Error: status " + status);
                    }
                );

            }
            else if(ctrl.questionDialog.metadata.state == QuestionsCrudFactory.STATES.APPROVED){
                $window.alert("The question has already been approved");
            }
            else{
                $window.alert("The Question cannot be sent for approval");
            }


        };

    }

})();
