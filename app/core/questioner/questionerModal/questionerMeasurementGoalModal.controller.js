(function() { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name QuestionerMeasurementGoalModalCtrl
* @module metricapp
* @requires $scope
* @requires $location
************************************************************************************/

angular.module('metricapp')

.controller('QuestionerMeasurementGoalModalCtrl', QuestionerMeasurementGoalModalCtrl);

QuestionerMeasurementGoalModalCtrl.$inject = ['$location', '$uibModalInstance', '$window', 'MeasurementGoalService', 'QuestionStorageFactory', 
    'QuestionsCrudFactory'];

function QuestionerMeasurementGoalModalCtrl($location, $uibModalInstance, $window, MeasurementGoalService, QuestionStorageFactory, 
    QuestionsCrudFactory) {

        var ctrl = this;

        ctrl.mgDialog = angular.copy(QuestionStorageFactory.getMeasurementGoal());
        ctrl.stateIsWaiting = ctrl.mgDialog.metadata.state == QuestionsCrudFactory.STATES.ONUPDATEWAITING;
        ctrl.showReleaseNote = false;

        ctrl.closeModal = function(){
            $uibModalInstance.dismiss("closing");            
            ctrl.mgDialog = angular.copy(QuestionStorageFactory.getMeasurementGoal());
        };

        ctrl.showQuestions = function(){
        	$uibModalInstance.dismiss("closing"); 
        	QuestionStorageFactory.setMeasurementGoal(ctrl.mgDialog);
        	$location.path('/measurementGoalQuestionList');
        };

        ctrl.addExistingQuestion = function(){
        	$uibModalInstance.dismiss("closing");
        	QuestionStorageFactory.setMeasurementGoal(ctrl.mgDialog); 
        	$location.path('/measurementGoalAddExistingQuestion');
        };

        ctrl.askForQuestionEndpoint = function(){
            ctrl.mgDialog.metadata.state = QuestionsCrudFactory.STATES.ONUPDATEENDPOINT;
            MeasurementGoalService.submitMeasurementGoal(ctrl.mgDialog);
            QuestionStorageFactory.setMeasurementGoal(ctrl.mgDialog);
            ctrl.stateIsWaiting = false;
            ctrl.showReleaseNote = false;
        };

        ctrl.inputReleaseNote = function(){
            ctrl.showReleaseNote = true;
        };

    }

})();
