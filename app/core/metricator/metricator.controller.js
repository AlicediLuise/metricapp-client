/*
* @Author: alessandro.fazio
* @Date:   2016-06-14 15:53:20
* @Last Modified by:   alessandro.fazio
* @Last Modified time: 2016-07-19 16:38:48
*/
(function () { 'use strict';

/************************************************************************************
* @ngdoc controller
* @name MetricatorController
* @module metricapp
* @requires $scope
* @requires $location
* @requires MetricService
* @requires MeasurementGoalService
* @requires $window
* @requires STATES
* @requires AuthService
* @requires MeasurementGoalModalService
* @description
* Manages the MeasurementGoal Page.
* Realizes the control layer for `metricator.view`.
************************************************************************************/
angular.module('metricapp')

.controller('MetricatorController', MetricatorController);

MetricatorController.$inject = ['$scope', '$location','MetricService','MeasurementGoalService','$window','STATES', 'AuthService', 'MeasurementGoalModalService'];

function MetricatorController($scope, $location, MetricService, MeasurementGoalService, $window, STATES, AuthService, MeasurementGoalModalService) {

    var vm = this;

    //vm.states = [STATES.ONUPDATEENDPOINT, STATES.ONUPDATEWAITING];
    //vm.measurementGoals = [undefined,undefined];
    vm.measurementGoals = [];
    vm.metrics = [];
    vm.contextFactors = [];
    vm.assumptions = [];
    vm.organizationalGoal = {};
    vm.instanceProject = {};
    vm.measurementGoalDialog = {};
    vm.modalIds = [];

    vm.setMeasurementGoalDialog = setMeasurementGoalDialog;
    vm.getMeasurementGoalsBy = getMeasurementGoalsBy;
    vm.goToUpdateMeasurementGoal = goToUpdateMeasurementGoal;
    vm.getMeasurementGoalExternals = getMeasurementGoalExternals;

    _getMeasurementGoals();
    _init();

    /********************************************************************************
    * @ngdoc method
    * @name _getMeasurementGoals
    * @description
    * Get active measurement goals for a metricator.
    ********************************************************************************/
    function _getMeasurementGoals(){
        //for (var i=0; i<vm.states.length; i++){
        //    getMeasurementGoalsByState(i);
        //}
        MeasurementGoalService.getMyMeasurementGoals().then(
            function(data) {
               // console.log(data.measurementGoals);
                //vm.results.measurementGoals = data.measurementGoals;
                vm.measurementGoals = data.measurementGoals;
            },
            function(data) {
                alert('Error retriving Measurement Goals');
            }
        );
    };


    /********************************************************************************
    * @ngdoc method
    * @name getMeasurementGoalsBy
    * @description
    * Search for Measurement Goals by some field.
    * Every field is supported, the most common : 
    * - id
    * - userid
    * - object
    * - qualityFocus
    * - purpose
    * - viewPoint
    * - tag
    * - state
    ********************************************************************************/
    function getMeasurementGoalsBy(keyword,field){
        vm.loading = true;
        console.log("Inside getMeasurementGoalsBy");
        if (keyword != null && field != null){
            MeasurementGoalService.getMeasurementGoalsBy(keyword,field).then(
                function(data) {
                  //  console.log(data.measurementGoals);
                    vm.measurementGoals = data.measurementGoals;
                    if(vm.measurementGoals.length === 0)
                        $window.alert(data.error);
                    vm.loading = false;
                },
                function(data) {
                    alert('Error retriving Measurement Goals');
                    vm.loading = false;
                }
            );
        }
        else
            $window.alert("You must enter keyword and field");
            vm.loading = false;
    }

    /********************************************************************************
    * @ngdoc method
    * @name getMeasurementGoalExternals
    * @description
    * Get measurement goals externals:
    * - Reference to Metrics
    * - Reference to Questions
    * - Reference to ContextFactors
    * - Reference to Assumptions
    * - Reference to InstanceProject
    * - Reference to OrganizationalGoal
    * Push the new references into the cache based on [key,value] comparison.
    * Store the new references into the MeasurementGoalService.
    * Open a new Measurement Goal Modal.
    ********************************************************************************/
    function getMeasurementGoalExternals(externalId){

         MeasurementGoalService.getMeasurementGoalExternals(externalId).then(
            function(data) {
                vm.metrics = data.metrics;
                vm.contextFactors = data.contextFactors;
                vm.assumptions = data.assumptions;
                vm.organizationalGoal = data.organizationalGoal;
                vm.instanceProject = data.instanceProject;
                vm.questions = data.questions;
                vm.pendingQuestions = data.pendingQuestions;

	        	var toUpdate = {
	            	measurementGoal : vm.measurementGoalDialog,
	            	metrics : data.metrics,
	            	contextFactors : data.contextFactors,
	            	assumptions : data.assumptions,
	            	organizationalGoal : data.organizationalGoal,
	            	instanceProject : data.instanceProject,
                    questions : data.questions,
                    pendingQuestions : data.pendingQuestions
	        	}; 	

                vm.modalIds.push([externalId,toUpdate]);

	        	//Send to MeasurementGoalService to open a modal
        		MeasurementGoalService.toUpdateMeasurementGoal(toUpdate);
	        	MeasurementGoalModalService.openMeasurementGoalModal();
                console.log("Retrieving Externals");
               // console.log(data.questions);
            },
            function(data) {
                alert('Error retriving Externals');
        	}
        );
    };

    /********************************************************************************
    * @ngdoc method
    * @name setMeasurementGoalDialog
    * @description
    * Sets the right Measurement Goal Modal to see MG details.
    * Cache strategy based on [key,value] comparison.
    * If no other items like toSearchId in cache :
    * - Store the new references into the MeasurementGoalService.
    * - Open a new Measurement Goal Modal.
    ********************************************************************************/
    function setMeasurementGoalDialog(measurementGoalToAssignId){
        //vm.measurementGoalDialog = vm.measurementGoals[parentIndex][measurementGoalToAssignId];
        vm.measurementGoalDialog = vm.measurementGoals[measurementGoalToAssignId];
        //var toSearchId = vm.measurementGoals[parentIndex][measurementGoalToAssignId].metadata.id;
        var toSearchId = vm.measurementGoals[measurementGoalToAssignId].metadata.id;
        var doubleInCache = false;

        for (var t=0, length = vm.modalIds.length; t<length; t++){
            if (vm.modalIds[t][0] === toSearchId){
                //Send to MeasurementGoalService to open a modal
                console.log("DOUBLE IN CACHE");
                MeasurementGoalService.toUpdateMeasurementGoal(vm.modalIds[t][1]);
                MeasurementGoalModalService.openMeasurementGoalModal();
                doubleInCache = true;
                break;
            }
        }

        if (!doubleInCache){ 
            console.log("NOT DOUBLE IN CACHE");
            getMeasurementGoalExternals(toSearchId);
        }
    };

    /********************************************************************************
    * @ngdoc method
    * @name goToUpdateMeasurementGoal
    * @description
    * Pass along the toUpdate object to MeasurementGoalService.
    * In this way we can retrieve the MeasurementGoal and it's references
    * in other pages (i.e. measurementgoal.view).
    ********************************************************************************/
    function goToUpdateMeasurementGoal(){

        var toUpdate = {
            measurementGoal : vm.measurementGoalDialog,
            metrics : vm.metrics,
            contextFactors : vm.contextFactors,
            assumptions : vm.assumptions,
            organizationalGoal : vm.organizationalGoal,
            instanceProject : vm.instanceProject,
            questions : vm.questions
        };

        MeasurementGoalService.toUpdateMeasurementGoal(toUpdate);
        vm.modalIds = [];
        $location.path('/measurementgoal');
    }

    /********************************************************************************
    * @ngdoc method
    * @name isModifiable
    * @description
    * Measurement Goal can be updated if is assigned to me.
    ********************************************************************************/
    function isModifiable(){
        return vm.measurementGoalDialog.metricatorId == AuthService.getUser().username;
    }

    /********************************************************************************
    * @ngdoc method
    * @name isSubmittable
    * @description
    * Measurement Goal can be submitted.
    ********************************************************************************/ 
    function isSubmittable(){
        return vm.measurementGoalDialog.metricatorId == AuthService.getUser().username && vm.measurementGoalDialog.metadata.state == 'OnUpdateQuestionerEndpoint';
    }

    /********************************************************************************
    * @ngdoc method
    * @name _init
    * @description
    * Initializes the controller.
    ********************************************************************************/
    function _init() {
        vm.loading = false;
    }
    }
    
})();
