(function() { 'use strict';

/************************************************************************************
* @ngdoc service
* @name NotificationService
* @module metricapp
* @requires $http
* @requires REST_SERVICE
* @requires AuthService
*
* @description
* Provides notifications management services.
************************************************************************************/

angular.module('metricapp')

.service('NotificationService', NotificationService);

NotificationService.$inject = ['$http', '$q', 'REST_SERVICE', 'AuthService', 'DB_NOTIFICATIONS'];

function NotificationService($http, $q, REST_SERVICE, AuthService, DB_NOTIFICATIONS) {

    var service = this;

    service.getAll = getAll;
    service.getNews = getNews;
    service.getPage = getPage;

    service.setReadById = setReadById;
    service.setAllRead = setAllRead;

    /********************************************************************************
    * @ngdoc method
    * @name getAll
    * @description
    * Retrieves all the notifications for authuser;
    * @returns {[Notification]|Error} On success, the notifications;
    * an error message, otherwise.
    ********************************************************************************/
    function getAll() {
        var request = {
            method: 'GET',
            url: REST_SERVICE.URL + '/notifications',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        return $http(request).then(
            function(success) {
                var data = angular.fromJson(success.data);
                //var notifications = angular.fromJson(data.notificationsDTO);
                var notifications = angular.copy(data.notificationsDTO);
                //alert('NotificationService.getAll : ' + notifications);
                var toread = 0;
                notifications.forEach(function(notification) {
                    var author = {
                        username: notification.authorId || '#',
                        firstname: notification.metadata.userFirstname,
                        lastname: notification.metadata.userLastname,
                        picture: notification.metadata.userPicture
                    };
                    notification.author = angular.copy(author);
                    if (!notification.read) {
                        toread++;
                    }
                });
                return $q.resolve({notifications: notifications, toread: toread});
            },
            function(error) {
                return $q.reject({errmsg: 'Error on server'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name getNews
    * @description
    * Retrieves all the new notifications for authuser;
    * @returns {[Notification]|Error} On success, the new notifications;
    * an error message, otherwise.
    ********************************************************************************/
    function getNews() {
        var request = {
            method: 'GET',
            url: REST_SERVICE.URL + '/notifications/inbox',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        return $http(request).then(
            function(success) {
                var data = angular.fromJson(success.data);
                var notifications = angular.copy(data.notificationsDTO);
                var news = data.count;
                notifications.forEach(function(notification) {
                    var author = {
                        username: notification.authorId || '#',
                        firstname: notification.metadata.userFirstname,
                        lastname: notification.metadata.userLastname,
                        picture: notification.metadata.userPicture
                    };
                    notification.author = angular.copy(author);
                });
                return $q.resolve({notifications: notifications, news: news});
            },
            function(error) {
                return $q.reject({errmsg: 'Error on server'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name getPage
    * @description
    * Retrieves the specified notifications page for authuser.
    * @param {Int} nFrom First notification index.
    * @param {Int} nSize Number of notifications.
    * @returns {[Notification]|Error} On success, the notifications page for authuser;
    * an error message, otherwise.
    ********************************************************************************/
    function getPage(from, size) {
        var request = {
            method: 'GET',
            url: REST_SERVICE.URL + '/notifications',
            headers: {
                'Content-Type': 'application/json'
            },
            params: {
                from: nFrom,
                size: nSize
            }
        };
        return $http(request).then(
            function(success) {
                var data = angular.fromJson(success.data);
                var notifications = angular.copy(data.notificationsDTO);
                notifications.forEach(function(notification) {
                    var author = {
                        username: notification.authorId || '#',
                        firstname: notification.metadata.userFirstname,
                        lastname: notification.metadata.userLastname,
                        picture: notification.metadata.userPicture
                    };
                    notification.author = angular.copy(author);
                    if (!notification.read) {
                        toread++;
                    }
                });
                return $q.resolve({notifications: notifications, toread: toread});
            },
            function(error) {
                return $q.reject({errmsg: 'Error on server'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name setReadById
    * @description
    * Set as read the specified notification.
    * @param {Int} notificationid The id of the notification to set read.
    * @returns {Boolean|Error} On success, a message;
    * an error message, otherwise.
    ********************************************************************************/
    function setReadById(notificationid) {
        var request = {
            method: 'PATCH',
            url: REST_SERVICE.URL + '/notifications',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: notificationid,
                read: true
            }
        };
        return $http(request).then(
            function(success) {
                return $q.resolve();
            },
            function(error) {
                return $q.reject({errmsg: 'Error on server'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name setAllRead
    * @description
    * Set as read all the notifications for authuser.
    * @returns {Boolean|Error} On success, a message;
    * an error message, otherwise.
    ********************************************************************************/
    function setAllRead() {
        var request = {
            method: 'PATCH',
            url: REST_SERVICE.URL + '/notifications',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: '*',
                read: true
            }
        };
        return $http(request).then(
            function(success) {
                return $q.resolve();
            },
            function(error) {
                return $q.reject({errmsg: 'Error on server'});
            }
        );
    }

}

})();
