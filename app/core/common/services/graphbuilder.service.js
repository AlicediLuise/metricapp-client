(function() { 'use strict';

/************************************************************************************
*
* @description
************************************************************************************/

angular.module('metricapp')

.service('GraphBuilderService',GraphBuilderService);

GraphBuilderService.$inject = ['MeasurementGoalService'];

function GraphBuilderService(MeasurementGoalService) {

    var service = this;
    service.buildGraph = buildGraph;
    service.elements = {};

    function buildGraph(){
      return MeasurementGoalService.getMyMeasurementGoals().then(
         function(data){
            service.mgs = data.measurementGoals;
           // console.log(service.mgs);
            _buildAfterGet();
           // console.log(service.elements);
            return service.elements;
         },function(data){console.log('Error');}
      );
   }

   //node_name:{ group: 'nodes', data:{ weight: 30,   color: 'orange' ,  name: 'Metric 1' }},
   function _add(type, id, instanceF){
      if(type=='OG'){
         service.elements[type+id] = {group:'nodes', data:{weight: 50, color: 'red', instance:instanceF}};
      }
      if(type=='MG'){
         service.elements[type+id] = {group:'nodes', data:{weight: 35, color: 'blue', instance:instanceF}};
      }
      if(type=='ME'){
         service.elements[type+id] = {group:'nodes', data:{weight: 45, color: 'orange', instance:instanceF}};
      }
      if(type=='QU'){
         service.elements[type+id] = {group:'nodes', data:{weight: 25, color: 'green', instance:instanceF}};
      }
   }


   function _addEdge(id1,type1, id2,type2){
      service.elements['ED'+id1+id2] = {group:'edges', data:{target:type2+id2, source:type1+id1}};
   }

   function _putMeasurementGoal(goal){
      _add('MG', goal.metadata.id, goal);
   }

   function _getQuestions(goal){
      //for(var question in goal.questions){
      for(var z=0, len=goal.questions.length;z<len;z++){ 
         _add('QU', goal.questions[z].instance, goal.questions[z]);
         _addEdge(goal.metadata.id, 'MG', goal.questions[z].instance, 'QU' );
      }
   }

   function _getMetrics(goal){
      //for(var metric in goal.metrics){
      for(var j=0, len=goal.metrics.length;j<len;j++){ 
         _add('ME', goal.metrics[j].instance, goal.metrics[j]);
         _addEdge(goal.metadata.id, 'MG', goal.metrics[j].instance, 'ME' );
      }
   }


   function _getOrganizationalGoal(goal){
         _add('OG', goal.organizationalGoalId.instance, goal.organizationalGoalId);
         _addEdge(goal.metadata.id, 'MG', goal.organizationalGoalId.instance, 'OG' );
      }



   function _buildAfterGet(){
      //for(var goal in service.mgs){
      for(var i=0, len=service.mgs.length;i<len;i++){ 
         _putMeasurementGoal(service.mgs[i]);
         _getQuestions(service.mgs[i]);
         _getMetrics(service.mgs[i]);
         _getOrganizationalGoal(service.mgs[i]);
      }

   }

}

})();
