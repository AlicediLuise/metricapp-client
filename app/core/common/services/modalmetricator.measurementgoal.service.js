(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc service
* @name ModalMeasurementGoalService
* @module metricapp
* @requires $window
* @requires $uibModal
* @description
* Service for MeasurementGoal Modals.
************************************************************************************/
.service('MeasurementGoalModalService', MeasurementGoalModalService);

MeasurementGoalModalService.$inject = ['$window', '$uibModal'];

function MeasurementGoalModalService($window, $uibModal) {

	var service = this;
	service.openMeasurementGoalModal = openMeasurementGoalModal;

	function openMeasurementGoalModal(){
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.wrapper.measurementgoal.view.html',
            controller: 'MeasurementGoalModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	};

}

})();
