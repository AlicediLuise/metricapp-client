(function() { 'use strict';
  
/************************************************************************************
 * @ngdoc service
 * @name GraphService
 * @module metricapp
 * @requires $http
 * @requires REST_SERVICE
 * @requires AuthService
 *
 * @description
 * Provides graph management services.
 * @requires $window
 * @requires $cookies
 *
 * @description
 * Get Graphs.
 
 ************************************************************************************/
  
angular.module('metricapp')
  
.service('GraphService', GraphService);

GraphService.$inject = ['$http', '$q', 'REST_SERVICE', 'AuthService', 'DB_METRICS', '$window', 'FlashService'];
 
function GraphService($http,  $q, REST_SERVICE, AuthService, DB_METRICS,$window,FlashService) {
  
  var service = this;
  service.graphToUpdate = {};
  service.getAllGraphs = getAllGraphs;
  service.getGraphByMeasurementGoalId = getGraphByMeasurementGoalId;
  service.toUpdateGraph = toUpdateGraph;
  service.getUpdateGraph = getUpdateGraph;
  service.submitGraph = submitGraph;
  service.deleteGraphById = deleteGraphById;
 
 
 /********************************************************************************
  * @ngdoc method
  * @name getAllGraphs
  * @description
  ********************************************************************************/
 function getAllGraphs() {
        console.log('GET Graphs');
 
        return $http.get('http://160.80.1.219/metricapp-server/graph').then(
                        function(response) {
                            var message = angular.fromJson(response.data);
                            console.log('SUCCESS GET GRAPHS');
                            return message;
                        },
                        function(response) {
                            var message = angular.fromJson(response.data);
                            console.log('FAILURE GET GRAPHS');
                            console.log(message);
                            return message;
                        }
        );
 }
 
 function getGraphByMeasurementGoalId(measurementGoalId) {
        return $http.get('http://160.80.1.219/metricapp-server/graph?measurementGoalId=' + measurementGoalId).then(
                        function(response) {
                            var message = angular.fromJson(response.data);
                            console.log('SUCCESS GRAPH');
                            return message;
                        },
                        function(response) {
                            var message = angular.fromJson(response.data);
                            console.log('FAILURE GRAPH');
                            console.log(message);
                            return message;
                        }
        );

 }
 
 function deleteGraphById(id) {
    return $http.delete('http://160.80.1.219/metricapp-server/graph?id=' + id).then(
                function(response) {
                    console.log('SUCCESS DELETE GRAPH');
                    return response;
                },
                function(response) {
                    console.log('FAILURE DELETE GRAPH');
                    return message;
                }
    );
 
 }

 
 function toUpdateGraph(graphToUpdate) {
    service.graphToUpdate = graphToUpdate;
 }
 
 function getUpdateGraph() {
    return service.graphToUpdate;
 }
 
 function submitGraph(graph) {
 
 
    console.log("PUT GRAPH");
    console.log(JSON.stringify(graph));
 
    return $http.put('http://160.80.1.219/metricapp-server/graph/', graph).then(
                    function(response) {
                        console.log('SUCCESS PUT graph');
                        var message = response.data;
                        console.log(message);
                        return message;
                    },
                    function(response) {
                        var message = response.data;
                        console.log('FAILURE PUT graph');
                        return message;
                    }
    );
 }
 
 
}
})();
