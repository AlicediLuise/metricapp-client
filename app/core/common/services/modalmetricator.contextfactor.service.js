(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc service
* @name ContextFactorModalService
* @module metricapp
* @requires $window
* @requires $uibModal
* @description
* Service for ContextFactor Modals.
************************************************************************************/
.service('ContextFactorModalService', ContextFactorModalService);

ContextFactorModalService.$inject = ['$window', '$uibModal'];

function ContextFactorModalService($window, $uibModal) {

	var service = this;
	service.openContextFactorModal = openContextFactorModal;
    service.openExternalContextFactorModal = openExternalContextFactorModal;

	function openContextFactorModal(){
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.contextfactor.view.html',
            controller: 'ContextFactorModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	};

    /********************************************************************************
    * @ngdoc method
    * @name openExternalContextFactorModal
    * @description
    * Opens an External ContextFactor Modal.
    * This function is reached when it's necessary to grab references of 
    * External Context Factors to add them to a MeasurementGoal
    ********************************************************************************/
	function openExternalContextFactorModal(){
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.external.contextfactor.view.html',
            controller: 'ExternalContextFactorModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	};

}

})();
