(function() { 'use strict';

/************************************************************************************
* @ngdoc service
* @name UserService
* @module metricapp
* @requires $http
* @requires $cookies
* @requires REST_SERVICE
* @requires AuthService
* @requires ROLES
* @requires GENDERS
*
* @description
* Provides users management services.
************************************************************************************/

angular.module('metricapp')

.service('UserService', UserService);

UserService.$inject = ['$http', '$q', '$cookies', 'REST_SERVICE', 'AuthService',
'ROLES', 'GENDERS', 'DB_USERS'];

function UserService($http, $q, $cookies, REST_SERVICE, AuthService, ROLES, GENDERS, DB_USERS) {

    var service = this;

    service.ROLES = ROLES;
    service.GENDERS = GENDERS;

    service.getAll = getAll;
    service.getById = getById;
    service.isUser = isUser;
    service.getInArray = getInArray;
    service.getNFrom = getNFrom;

    service.create = create;
    service.update = update;

    /********************************************************************************
    * @ngdoc method
    * @name getAll
    * @description
    * Retrieves all the users.
    * @returns {[User]|Error} On success, the users;
    * an error message, otherwise.
    ********************************************************************************/
    function getAll() {
        //console.log('GET ALL USERS');
        var request = {
            method: 'GET',
            url: REST_SERVICE.URL + '/user',
            params: {
                username: 'all'
            }
        };
        return $http(request).then(
            function(success) {
                var data = angular.fromJson(success.data);
                var usersDTO = angular.copy(data.userList);
                var users = [];
                usersDTO.forEach(function(userDTO) {
                    var user = angular.copy(userDTO);
                    delete user.metadata;
                    delete user.password;
                    users.push(user);
                });
                return $q.resolve({users: users});
            },
            function(error) {
                return $q.reject({errmsg:'Erro on server'});
            }
        );
/*
        return $http.get('http://160.80.1.219/metricapp-server/user?username=all').then(
            function(response) {
                //var users = [];
                //for (var user in DB_USERS) {
                //    users.push(DB_USERS[user]);
                //}
                var message = angular.fromJson(response.data.userList);
                console.log('USERS');
                console.log(message);
                return message;
            },
            function(response){
                console.log(response.error);
            }
        );
        */
    }

    /********************************************************************************
    * @ngdoc method
    * @name getById
    * @description
    * Retrieves the specified user.
    * @param {String} username The username of the user to retrieve.
    * @returns {User|Error} On success, the user;
    * an error message, otherwise.
    ********************************************************************************/
    function getById(user) {
        var request = {method: 'GET', url:'http://160.80.1.219/metricapp-server/user', params:{username: user}};

        return $http(request).then(
            function(success)
            {
                var message = angular.fromJson(success.data);
                return $q.resolve({user: message.userList[0]});
            },
            function(error)
            {
                return $q.reject({errmsg: 'User ' + username + ' not found'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name getById
    * @description
    * Retrieves the specified user.
    * @param {String} username The username of the user to retrieve.
    * @returns {User|Error} On success, the user;
    * an error message, otherwise.
    ********************************************************************************/
    function isUser(username) {
        var request = {
            method: 'GET',
            url: 'http://160.80.1.219/metricapp-server/user',
            //url: REST_SERVICE.URL + '/user',
            params: {
                username: username
            }
        };

        return $http(request).then(
            function(success) {
                alert('Success GET USERID');
                return $q.resolve();
            },
            function(error) {
                alert('Failure GET USERID'); 
                return $q.reject();
            }
        );
        /*
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var USER = DB_USERS[username];
                if (USER) {
                    resolve();
                } else {
                    reject();
                }
            }, 500);
        });
        */
    }

    /********************************************************************************
    * @ngdoc method
    * @name getInArray
    * @description
    * Retrieves the specified users.
    * @param {[String]} usernames Usernames of users to retrieve.
    * @returns {{username:User}|Error} On success, the list of users;
    * an error message, otherwise.
    ********************************************************************************/
    function getInArray(usernames) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var users = {};
                usernames.forEach(function(username) {
                    var USER = DB_USERS[username];
                    if (USER) {
                        users[username] = USER;
                    }
                });
                resolve({users: users});
            }, 500);
        });
    }

    /********************************************************************************
    * @ngdoc method
    * @name getNFrom
    * @description
    * Retrieves the specified users.
    * @param {Int} usrStart First user index.
    * @param {Int} usrN Number of users.
    * @returns {[User]|Error} On success, the list of users;
    * an error message, otherwise.
    ********************************************************************************/
    function getNFrom(usrStart, usrN) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var users = [];
                var numusers =  Object.keys(DB_USERS).length;
                var end = (usrN === -1) ? numusers : Math.min(usrStart + usrN, numusers);
                for (var i = usrStart; i < end; i++ ) {
                    users.push(DB_USERS[i]);
                }
                resolve({users: users, numusers: numusers});
            }, 500);
        });
    }

    /********************************************************************************
    * @ngdoc method
    * @name create
    * @description
    * Creates a new user.
    * @param {User} user The user to create.
    * @returns {JSON|Error} On success, the username of the successfully created user
    * and a success message;
    * an error message, otherwise.
    ********************************************************************************/
    function create(user) {
        var req = {
            method: 'POST',
            url: REST_SERVICE.URL + '/user',
            headers: {
                'Content-Type': 'application/json'
            },
            data: user
        };
        return $http(req).then(
            function(success) {
                return $q.resolve();
            },
            function(error) {
                return $q.reject({errmsg:'Erro on server'});
            }
        );
    }

    /********************************************************************************
    * @ngdoc method
    * @name update
    * @description
    * Updates the authuser profile.
    * @param {UserAttrs} userAttrs The user attributes to update.
    * @returns {String|Error} On success, the username of the updated user;
    * an error message, otherwise.
    ********************************************************************************/
    function update(userAttrs) {
        var userUpgrade =
        {
            username: userAttrs.username,
            lastname: userAttrs.lastname,
            firstname: userAttrs.firstname,

            mobile: userAttrs.mobile,
            website: userAttrs.website,
            role: userAttrs.role,

            gender: userAttrs.gender,
            pic: userAttrs.pic,
            bio: userAttrs.bio,
            email: userAttrs.email

        };
        return $http.put('http://160.80.1.219/metricapp-server/user', userUpgrade).then(
            function(success)
            {
                var message = angular.fromJson(success.data);
                return $q.resolve({user: message.userList[0]});
            },
            function(error)
            {
                return $q.reject({errmsg: 'User ' + username + ' not found'});
            }
        );
    }

}

})();
