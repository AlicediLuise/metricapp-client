(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc service
* @name MetricModalService
* @module metricapp
* @requires $window
* @requires $uibModal
* @description
* Service for Metric Modals.
************************************************************************************/
.service('MetricModalService', MetricModalService);

MetricModalService.$inject = ['$window', '$uibModal'];

function MetricModalService($window, $uibModal) {

	var service = this;
	service.openMetricModal = openMetricModal;
	service.openExternalMetricModal = openExternalMetricModal;

	function openMetricModal(){
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.metric.view.html',
            controller: 'MetricModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	};

    /********************************************************************************
    * @ngdoc method
    * @name submitMeasurementGoal
    * @description
    * Opens an External Metric Modal.
    * This function is reached when it's necessary to grab references of 
    * External Approved Metrics to add them to a MeasurementGoal
    ********************************************************************************/
  /*  function openExternalMetricModal(){
         console.log(className);
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.external.metric.view.html',
            controller: 'ExternalMetricModalCtrl',
            controllerAs: 'vm',
            size: 'lg',
            resolve: {
                    className: function () {
                                    return MeasurementGoal;
                                }
            }
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	}; */
 
 
    function openExternalMetricModal(className, metricInstance){
        var modalInstance = $uibModal.open({
                                    
                    templateUrl: 'dist/views/metricator/modal/modal.external.metric.view.html',
                    controller: 'ExternalMetricModalCtrl',
                    controllerAs: 'vm',
                    size: 'lg',
                    resolve: {
                                className: function () { return className },
                                metricInstance: function() { return metricInstance; }
                        }
                    });
 
        modalInstance.result.then(
                    function(){
                           console.log("Modal showing");
                        },
                    function () {
                           console.log('Modal dismissed');
                        });
	};

}

})();
