(function() { 'use strict';

/************************************************************************************
* @ngdoc service
* @name StateMachineMeasurementGoalService
* @module metricapp
* @requires AuthService
* @description
* Service for MeasurementGoal State Machine.
************************************************************************************/
angular.module('metricapp')

.service('StateMachineMeasurementGoalService', StateMachineMeasurementGoalService);

StateMachineMeasurementGoalService.$inject = ['AuthService', 'STATES', 'ROLES'];

function StateMachineMeasurementGoalService(AuthService, STATES, ROLES) {

    var service = this;
    service.measurementGoalDialog = {};
    service.isModifiable = isModifiable;
    service.isToAssign = isToAssign;
    service.isToReject = isToReject;
    service.isForApproval = isForApproval;
    service.isToChangeRequest = isToChangeRequest;
    service.isToApprove = isToApprove;
    service.noMetricatorsForMG = noMetricatorsForMG;
    service.noQuestionersForMG = noQuestionersForMG;
    service.setRightMeasurementGoalDialog = setRightMeasurementGoalDialog;
    service.isToAddQuestioner = isToAddQuestioner;

    /********************************************************************************
    * @ngdoc method
    * @name _setRightMeasurementGoalDialog
    * @description
    * Sets the right Measurement Goal Dialog to retrieve state .
    ********************************************************************************/ 
    function setRightMeasurementGoalDialog(rightMeasurementGoalDialog){
        service.measurementGoalDialog = rightMeasurementGoalDialog;//MeasurementGoalService.getUpdateMeasurementGoal().measurementGoal;    
    }

    /********************************************************************************
    * @ngdoc method
    * @name isModifiable
    * @description
    * Measurement Goal can be updated.
    ********************************************************************************/
    function isModifiable(){
        //Check if is mine
        if (AuthService.getUser().role != ROLES.METRICATOR ||
            service.measurementGoalDialog.metricatorId != AuthService.getUser().username){ 
            console.log('You are not a metricator or the MG is not assigned to you');
            return false;
        }

        //Check if there's a transitional state
        if (service.measurementGoalDialog.metadata.state == STATES.CREATED ||
            service.measurementGoalDialog.metadata.state == STATES.PENDING || 
            service.measurementGoalDialog.metadata.state == STATES.SUSPENDED){
            console.log('The MG is not in the state of Update');
            return false;
        }
        
        return true;
    }

    /********************************************************************************
    * @ngdoc method
    * @name isForApproval
    * @description
    * Measurement Goal can be sent for approval.
    ********************************************************************************/ 
    function isForApproval(){
        return AuthService.getUser().role == ROLES.METRICATOR &&
        service.measurementGoalDialog.metricatorId == AuthService.getUser().username && 
        service.measurementGoalDialog.metadata.state == STATES.ONUPDATEENDPOINT &&
        service.measurementGoalDialog.metrics.length > 0;
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToReject
    * @description
    * Measurement Goal can be rejected.
    ********************************************************************************/ 
    function isToReject(){
        return AuthService.getUser().role == ROLES.EXPERT && 
        service.measurementGoalDialog.metadata.state == STATES.PENDING;
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToApproved
    * @description
    * Measurement Goal can be approved.
    ********************************************************************************/ 
    function isToApprove(){
        return AuthService.getUser().role == ROLES.EXPERT && 
        service.measurementGoalDialog.metadata.state == STATES.PENDING;
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToChangeRequest
    * @description
    * Measurement Goal can be affected by change request.
    ********************************************************************************/ 
    function isToChangeRequest(){
        return AuthService.getUser().role == ROLES.EXPERT && 
        (service.measurementGoalDialog.metadata.state == STATES.APPROVED ||
        service.measurementGoalDialog.metadata.state == STATES.REJECTED);
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAssign
    * @description
    * Measurement Goal can be assigned.
    ********************************************************************************/ 
    function isToAssign(){
        return AuthService.getUser().role == ROLES.EXPERT && ( 
        service.measurementGoalDialog.metadata.state == STATES.CREATED);// ||
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAddQuestioner
    * @description
    * Can add a questioner to a Measurement Goal.
    ********************************************************************************/
    function isToAddQuestioner(){
        return AuthService.getUser().role == ROLES.EXPERT &&  
        service.measurementGoalDialog.metadata.state == STATES.ONUPDATEWAITING;    
    }

    /********************************************************************************
    * @ngdoc method
    * @name noMetricatorsForMG
    * @description
    * This function checks if a Measurement Goal contains a metricatorId.
    ********************************************************************************/ 
    function noMetricatorsForMG(){
        return service.measurementGoalDialog.metricatorId == undefined;
    }

    /********************************************************************************
    * @ngdoc method
    * @name noQuestionersForMG
    * @description
    * This function checks if a Measurement Goal contains a questionerId.
    ********************************************************************************/ 
    function noQuestionersForMG(){   
        return service.measurementGoalDialog.questionersId.length == 0;
    }

}

})();
