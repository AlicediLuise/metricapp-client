(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc service
* @name AssumptionModalService
* @module metricapp
* @requires $window
* @requires $uibModal
* @description
* Service for Assumption Modals.
************************************************************************************/
.service('AssumptionModalService', AssumptionModalService);

AssumptionModalService.$inject = ['$window', '$uibModal'];

function AssumptionModalService($window, $uibModal) {

	var service = this;
	service.openAssumptionModal = openAssumptionModal;
    service.openExternalAssumptionModal = openExternalAssumptionModal;

	function openAssumptionModal(){
	     var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.assumption.view.html',
            controller: 'AssumptionModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
	};

    /********************************************************************************
    * @ngdoc method
    * @name openExternalAssumptionModal
    * @description
    * Opens an External Assumption Modal.
    * This function is reached when it's necessary to grab references of 
    * External Assumption to add them to a MeasurementGoal
    ********************************************************************************/
    function openExternalAssumptionModal(){
         var modalInstance = $uibModal.open({

            templateUrl: 'dist/views/metricator/modal/modal.external.assumption.view.html',
            controller: 'ExternalAssumptionModalCtrl',
            controllerAs: 'vm',
            size: 'lg'
        });

        modalInstance.result.then(
            function(){
                console.log("Modal showing");
            },
            function () {
                console.log('Modal dismissed');
            });
    };

}

})();
