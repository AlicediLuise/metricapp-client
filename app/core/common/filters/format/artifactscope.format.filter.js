(function() { 'use strict';

/************************************************************************************
* @ngdoc filter
* @name artifactScopeFormat
* @module metricapp
*
* @description
* Formats the url from notification artifacts scope and id.
************************************************************************************/

angular.module('metricapp')

.filter('artifactScopeFormat', artifactScopeFormat);

function artifactScopeFormat() {

    return function(artifactScope) {

        switch(artifactScope) {
            case 'GRID':
                return 'grids';
            case 'OGOAL':
                return 'organizationalgoal';
            case 'MGOAL':
                return 'measurementgoal';
            case 'METRIC':
                return 'metrics';
            case 'QUESTION':
                return 'questions';
            case 'TEAM':
                return 'teams';
            case 'USER':
                return 'users';
            default:
                return artifactScope;
        }
    };
}

})();
