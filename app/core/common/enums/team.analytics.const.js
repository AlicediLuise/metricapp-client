(function() { 'use strict';

/************************************************************************************
* @ngdoc constant
* @name TEAM_ANALYTICS
* @module metricapp
* @description
* Defines constants related to teams analytics.
* - GQM_SCORE:
* - GRIDS_TOT:
* - ARTIFACTS_TOT:
* - ACCEPTANCE_RATIO:
* - ARTIFACTS_MGOAL:
* - ARTIFACTS_RATE:
* - WORKTIME_RATIO:
* - TASKS_PROGRESS:
* - TASKS_RECORD:
************************************************************************************/

angular.module('metricapp')

.constant('TEAM_ANALYTICS', {
    GQM_SCORE:          'gqmScore',
    GRIDS_TOT:          'gridsTot',
    ARTIFACTS_TOT:      'artifactsTot',
    ACCEPTANCE_RATIO:   'acceptanceRatio',
    ARTIFACTS_MGOAL:    'artifactsMGoal',
    ARTIFACTS_RATE:     'artifactsRate',
    WORKTIME_RATIO:     'worktimeRatio',
    TASKS_PROGRESS:     'tasksProgress',
    TASKS_RECORD:       'tasksRecord',
    TASKS_RECORD_ASS:   'assigned',
    TASKS_RECORD_SUB:   'submitted',
    TASKS_RECORD_ACC:   'accepted'
});

})();
