(function() { 'use strict';

/************************************************************************************
* @ngdoc overview
* @name notificationDaemon
* @module metricapp
* @requires $rootScope
* @requires $interval
* @requires NotificationService
* @requires AuthService
* @requires UserService
* @requires AUTH_EVENTS
* @requires NOTIFICATION_EVENTS
*
* @description
* Pulls notifications as a daemon.
************************************************************************************/

angular.module('metricapp')

.run(notificationDaemon);

notificationDaemon.$inject = ['$rootScope', '$interval', 'NotificationService', 'AuthService', 'UserService', 'AUTH_EVENTS', 'NOTIFICATION_EVENTS'];

function notificationDaemon($rootScope, $interval, NotificationService, AuthService, UserService, AUTH_EVENTS, NOTIFICATION_EVENTS) {

    _init();

    function _startPolling() {
        $rootScope.polling = $interval(function() {
            NotificationService.getNews().then(
                function(resolve) {
                    var newNotifications = angular.copy(resolve.notifications);
                    _broadcastNewNotifications(newNotifications);
                },
                function(reject) {
                    var errmsg = reject.errmsg;
                }
            );
        }, 3000);
    }

    function _stopPolling() {
        if ($rootScope.polling) {
            $interval.cancel($rootScope.polling);
        }
    }

    function _init() {
        if (AuthService.isLogged()) {
            _startPolling();
        }
    }

    function _broadcastNewNotifications(newNotifications) {
        $rootScope.$broadcast(NOTIFICATION_EVENTS.NEWS, newNotifications);
    }

    $rootScope.$on(AUTH_EVENTS.LOGIN_SUCCESS, function() {
        _startPolling();
    });

    $rootScope.$on(AUTH_EVENTS.LOGOUT_SUCCESS, function() {
        _stopPolling();
    });

}

})();
