(function() {  'use strict';

/************************************************************************************
* @ngdoc controller
* @name GridController
* @module metricapp
* @requires $scope
* @requires $rootScope
* @requires $location
* @requires $routeParams
* @requires $q
* @requires GridService
* @requires MGoalService
* @requires QuestionService
* @requires MetricService
* @requires UserService
* @requires ROLES
* @requires GRID_EVENTS
*
* @description
* Realizes the control layer for `grid.view`.
************************************************************************************/

angular.module('metricapp')

.controller('GridController', GridController);

GridController.$inject = ['$scope', '$rootScope', '$location', '$routeParams', '$q',
'GridService', 'MGoalService', 'QuestionService', 'MetricService', 'UserService', 'ROLES', 'GRID_EVENTS','cytoData', 'GraphBuilderService'];

function GridController($scope, $rootScope, $location, $routeParams, $q,
    GridService, MGoalService, QuestionService, MetricService, UserService, ROLES, GRID_EVENTS,cytoData, GraphBuilderService) {

    var vm = this;

    vm.updateGrid = updateGrid;
    vm.removeMember = removeMember;
    //vm.isOG = isOG;
    //vm.realId = '';

    _init();
    _buildEntireGraph();    
    
    function updateGrid(grid) {
        vm.loading = true;
        vm.success = false;
        GridService.update(grid).then(
            function(resolve) {
                vm.currGrid = angular.copy(resolve.grid);
                var gridid = resolve.grid.gridid;
                vm.success = true;
                $location.path('/grids/' + gridid);
                $rootScope.$broadcast(GRID_EVENTS.UPDATE_SUCCESS);
            },
            function(reject) {
                vm.errmsg = reject.errmsg;
                vm.success = false;
                $rootScope.$broadcast(GRID_EVENTS.UPDATE_FAILURE);
            }
        )
        .finally(function(){
            vm.loading = false;
        });
    }

    function removeMember(username, role) {
        if (role === ROLES.QUESTIONER) {
            delete vm.updtGrid.questioners[username];
        } else if (role === ROLES.METRICATOR) {
            delete vm.updtGrid.metricator[username];
        }
    }

    function _buildEntireGraph(){
        GraphBuilderService.buildGraph().then(
              function(data){
                vm.elements = data;
                console.log('Elements');
               // console.log($scope.vm.elements);
            },function(data){

            }
        );
    }

    //function isOG(){
    //    vm.realId = $scope.node_id.substring(2);
    //    return realId == 'OG';
    //}

    function _loadGrid(gridid) {
        vm.loading = true;
        vm.success = false;
        GridService.getById(gridid).then(
            function(resolve) {
                vm.currGrid = angular.copy(resolve.grid);
                var meta={
                    expert:UserService.getById(vm.currGrid.expert),
                    questioners:UserService.getInArray(vm.currGrid.questioners),
                    metricators:UserService.getInArray(vm.currGrid.metricators),
                    mgoals:MGoalService.getInArray(vm.currGrid.mgoals),
                    questions:QuestionService.getInArray(vm.currGrid.questions),
                    metrics:MetricService.getInArray(vm.currGrid.metrics)
                };
                return $q.all(meta).then(
                    function(resolve){
                        vm.currGrid.expert=angular.copy(resolve.expert.user);
                        vm.currGrid.questioners=angular.copy(resolve.questioners.users);
                        vm.currGrid.metricators=angular.copy(resolve.metricators.users);
                        vm.currGrid.mgoals=angular.copy(resolve.mgoals.mgoals);
                        vm.currGrid.questions=angular.copy(resolve.questions.questions);
                        vm.currGrid.metrics=angular.copy(resolve.metrics.metrics);
                        vm.updtGrid = angular.copy(vm.currGrid);
                        vm.num_questions = Object.keys(vm.currGrid.questions).length;
                        vm.num_mgoals = Object.keys(vm.currGrid.mgoals).length;
                        vm.num_metrics = Object.keys(vm.currGrid.metrics).length;
                        vm.success=true;
                    },
                    function(reject){
                        vm.errmsg = reject.errmsg;
                        alert(vm.errmsg);
                        vm.success = false;
                    }
                );
            },
            function(reject) {
                vm.errmsg = reject.errmsg;
                vm.success = false;
            }
        ).finally(function() {
            vm.loading = false;
        });
    }

    function _init() {
        vm.loading = true;
        vm.success = false;
        vm.errmsg = null;
        vm.currGrid = {
            id: $routeParams.gridid
        };
        _loadGrid(vm.currGrid.id);
    }

    //$scope.vm.options = {
    vm.options = {
        textureOnViewport:true,
        pixelRatio: 'auto',
        motionBlur: false,
        hideEdgesOnViewport:true
       };


    vm.layout = {name: 'cose'};

    vm.style =
    [
        {
            selector: 'node',
            style: {
                'height': 'data(weight)',
                'width': 'data(weight)',
                'shape': 'ellipse',
                'border-width': 2,
                'background-color': 'data(color)',
            }
        }
    ];

    $scope.node_name = '';
    $scope.node_type = '';
    $scope.node_id    = '';

    $scope.node_description = '';
    $scope.node_creation ='';
    $scope.node_update = '';

    $scope.events;
 
    $scope.$on('cy:node:click', function (ng, cy) {
        var node = cy.cyTarget;
       // console.log(node)

        $scope.temp = node.data().color;
        $scope.node_id = node.data().id.substring(2);

        if($scope.temp == 'blue'){
            $scope.node_type = 'Measurement Goal';

        }
        else if($scope.temp == 'red'){
            $scope.node_type = 'Organizational Goal';
        }
        else if($scope.temp == 'orange'){
            $scope.node_type = 'Metric';
        }
        else{
            $scope.node_type = 'Question';
        }
            
        $scope.node_name = node.data();

      //  console.log(node.data());
        $scope.$apply();
    });

    $scope.graph = {};
    
    cytoData.getGraph('core').then(function(graph){
        $scope.graph = graph;
    });

    $scope.panTo = function(){
        $scope.graph.pan({
            x: 100,
            y: 100
        })
    };
    $scope.center = function(){
        $scope.graph.center()
    };

}

})();
