(function () { 'use strict';
  
/************************************************************************************
 * @ngdoc controller
 * @name GraphController
 * @module metricapp
 * @requires $scope
 * @requires $location
 * @requires GraphService
 * @requires $window
 * @requires AuthService
 * @description
 * Manages the Graph Page.
 * Realizes the control layer for `graphs.view`.
 ************************************************************************************/
 angular.module('metricapp')
 
.controller('GraphController', GraphController);
 
GraphController.$inject = ['$scope', '$compile','$location', 'STATES','GraphService', 'MetricService', 'MetricModalService','QuestionService', 'QuestionStorageFactory', 'QuestionModalFactory', 'ContextFactorService', 'ContextFactorModalService', 'AssumptionService', 'AssumptionModalService', '$window', 'AuthService'];
 
function GraphController($scope, $compile, $location, STATES,  GraphService, MetricService, MetricModalService, QuestionService, QuestionStorageFactory, QuestionModalFactory, ContextFactorService, ContextFactorModalService, AssumptionService, AssumptionModalService, $window, AuthService) {

    var vm = this;
    
    vm.graph = {};
    vm.measurementGoal = {};
    vm.metrics = [];
    vm.contextFactors = [];
    vm.assumptions = [];
    vm.instanceProject = {};
 
    vm.setMetricDialog = setMetricDialog;
    vm.setMetricDialog = setMetricDialog;
    vm.setQuestionDialog = setQuestionDialog;
    vm.setContextFactorDialog = setContextFactorDialog;
    vm.setAssumptionDialog = setAssumptionDialog;
    vm.addBaselineHypotheses = addBaselineHypotheses;
    vm.submitGraph = submitGraph;
    vm.removeBaselineHypothesesFromGraph = removeBaselineHypothesesFromGraph;
    vm.removeVariationFactorsFromGraph = removeVariationFactorsFromGraph;
    vm.addVariationFactors = addVariationFactors;
 
    vm.contextFactorDialog = ContextFactorService.getContextFactorDialog();
 
    _init();
    _initGraphDialog();
 
    
    /********************************************************************************
     * @ngdoc method
     * @name _init
     * @description
     * Initializes the controller.
     ********************************************************************************/
    function _init() {
        vm.loading = false;
    }

    
    function _initGraphDialog(){
        vm.graph = GraphService.getUpdateGraph().graph;
        vm.measurementGoal = GraphService.getUpdateGraph().measurementGoal;
        vm.metrics = GraphService.getUpdateGraph().metrics;
        vm.contextFactors = GraphService.getUpdateGraph().contextFactors;
        vm.assumptions = GraphService.getUpdateGraph().assumptions;
        vm.instanceProject = GraphService.getUpdateGraph().instanceProject;
        vm.questions = GraphService.getUpdateGraph().questions;
        vm.pendingQuestions = GraphService.getUpdateGraph().pendingQuestions;
    }
 
    function setMetricDialog(index){
        var metricId = vm.measurementGoal.metrics[0].instance;
 
        MetricService.getMetricById(metricId).then(
            function(data) {
                MetricService.storeMetric(data);
                MetricModalService.openMetricModal();
            },
            function(data) {
                alert('Error retriving metric');
            }
        );

    }
 
    function setQuestionDialog(index, type){
        var questionId;
        if(type == "Question")
            questionId = vm.measurementGoal.questions[index].instance;
        else
            questionId = vm.measurementGoal.pendingQuestions[index].instance;
 
        QuestionService.getQuestionById(questionId).then(
            function(data) {
                QuestionStorageFactory.setQuestion(data.questionList[0]);
                QuestionModalFactory.openQuestionModal();
            },
            function(data) {
                alert('Error retriving question');
            }
        );
    }
 
    function setContextFactorDialog(index){
        var contextId = vm.measurementGoal.contextFactors[index].instance;
        ContextFactorService.getContextFactorById(contextId).then(
                function(data) {
                    ContextFactorService.storeContextFactor(data);
                    ContextFactorModalService.openContextFactorModal();
                },
                function(data) {
                    alert('Error retriving context factor');
                }
        );
    }
 
    function setAssumptionDialog(index){
        var assumptionId = vm.measurementGoal.assumptions[index].instance;
        AssumptionService.getAssumptionById(assumptionId).then(
                function(data) {
                    AssumptionService.storeAssumption(data);
                    AssumptionModalService.openAssumptionModal();
                },
                function(data) {
                    alert('Error retriving assumption');
                }
        );

    }
 
    function addBaselineHypotheses() {
 
    var number = $("[name='blh']").length;
    var blh = $("#blh");
    var value = "bvalues[" + number + "]";

    var html = '<div id="'+number+'_BaselineHypotheses"><div class="col-md-9"><input class="form-control" name="blh" type="text"></div><div class="col-md-1"><i ng-click="vm.removeBaselineHypothesesFromGraph('+value+')" class="glyphicon glyphicon-remove-circle pull-right"></i></div><br><br></div>';

    if($scope.bvalues == undefined)
        $scope.bvalues = [];
    $scope.bvalues[number] = number;
    angular.element(blh).append($compile(html)($scope));
 
    }
 
    function addVariationFactors() {
 
    var number = $("[name='vf']").length
    var vf = $("#vf");
    var value = "vvalues[" + number + "]";
 
    var html = '<div id="'+number+'_VariationFactors"><div class="col-md-9"><input class="form-control" name="vf" type="text"></div><div class="col-md-1"><i ng-click="vm.removeVariationFactorsFromGraph('+value+')" class="glyphicon glyphicon-remove-circle pull-right"></i></div><br><br></div>';
 
    if($scope.vvalues == undefined)
    $scope.vvalues = [];
    $scope.vvalues[number] = number;
    angular.element(vf).append($compile(html)($scope));
 
    }


    function removeBaselineHypothesesFromGraph(index) {
        var name = index + "_BaselineHypotheses";
        var item = $("#" + name).hide();
    }
 
    function removeVariationFactorsFromGraph(index) {
        var name = index + "_VariationFactors";
        var item = $("#" + name).hide();
    }
 
 /********************************************************************************
  * @ngdoc method
  * @name submitGraph
  * @description
  * Submits a Graph.
  ********************************************************************************/
 function submitGraph() {
 
    vm.graph.baselineHypotheses = [];
    vm.graph.variationFactors = [];
    vm.graph.metadata.state = STATES.ONUPDATE;
 
    var blh = $("[name='blh']").each(function() {
            if($(this).is(":visible") && this.value != "")
                vm.graph.baselineHypotheses.push(this.value);
    });
 
    var vf = $("[name='vf']").each(function() {
            if($(this).is(":visible") && this.value != "")
                vm.graph.variationFactors.push(this.value);
    });
 
    GraphService.submitGraph(vm.graph).then(
        function(message) {
            console.log(message);
            $window.alert('Graph Submitted');
            $location.path('/graphs');
        },
        function(message) {
            alert(message);
        }
    );

 }
 
    
}
})();
