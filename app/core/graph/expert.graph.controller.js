(function () { 'use strict';
  
/************************************************************************************
 * @ngdoc controller
 * @name ExpertGraphController
 * @module metricapp
 * @requires $scope
 * @requires $location
 * @requires GraphService
 * @requires $window
 * @requires AuthService
 * @description
 * Manages the Graph Page.
 * Realizes the control layer for `graphs.view`.
 ************************************************************************************/
angular.module('metricapp')
  
.controller('ExpertGraphController', ExpertGraphController);
  
ExpertGraphController.$inject = ['$scope', '$location','GraphService', '$window', 'AuthService', 'MeasurementGoalService'];
  
function ExpertGraphController($scope, $location, GraphService, $window, AuthService, MeasurementGoalService) {
  
  var vm = this;

  vm.graphs = [];
  vm.graph = {};
  vm.measurementGoal = {};
  vm.metrics = [];
  vm.contextFactors = [];
  vm.assumptions = [];
  vm.instanceProject = {};
  
  vm.goToUpdateGraph = goToUpdateGraph;
  
  _getGraphs();
  _init();
  
  /********************************************************************************
   * @ngdoc method
   * @name _getGraphs
   * @description
   * Get all graphs.
   ********************************************************************************/
  function _getGraphs(){
        GraphService.getAllGraphs().then(
            function(data) {
                vm.graphs = data.graphs;
            },
            function(data) {
                alert('Error retriving Graphs');
            }
        );
  };
  
  /********************************************************************************
   * @ngdoc method
   * @name _init
   * @description
   * Initializes the controller.
   ********************************************************************************/
  function _init() {
    vm.loading = false;
  }
  
  
  /********************************************************************************
   * @ngdoc method
   * @name goToUpdateGraph
   * @description
   ********************************************************************************/
  function goToUpdateGraph(graphIndex){
  
    vm.graph = vm.graphs[graphIndex];
    var toSearchId = vm.graph.measurementGoalId.instance;

    MeasurementGoalService.getMeasurementGoalsBy(toSearchId, "id").then(
        function(data) {
            var toUpdate = {
                graph : vm.graph,
                measurementGoal : data.measurementGoals[0],
            };
                                                                        
            GraphService.toUpdateGraph(toUpdate);
            $location.path('/graph');
            
    });
  }


}
})();
