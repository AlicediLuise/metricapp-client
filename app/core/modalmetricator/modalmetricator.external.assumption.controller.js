(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc controller
* @name ExternalAssumptionModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires MeasurementGoalService
* @requires AssumptionService
* @requires AssumptionModalService
* @requires $uibModalInstance
* @description
* Manages the External Assumption Modals.
* Realizes the control layer for `modal.external.assumption`.
************************************************************************************/
.controller('ExternalAssumptionModalCtrl', ExternalAssumptionModalCtrl);

ExternalAssumptionModalCtrl.$inject = ['$window', '$uibModal', 'AssumptionService', '$uibModalInstance', 'AssumptionModalService', 'MeasurementGoalService'];

function ExternalAssumptionModalCtrl($window, $uibModal, AssumptionService, $uibModalInstance, AssumptionModalService, MeasurementGoalService) {

	var vm = this;

	vm.externalAssumptionDialog = AssumptionService.getExternalAssumptionDialog();
	
	//console.log("External Metric Dialog");
	//console.log(vm.externalAssumptionDialog);

	vm.closeModal = closeModal;
	vm.setAssumptionDialog = setAssumptionDialog;
	vm.addAssumptionToMeasurementGoal = addAssumptionToMeasurementGoal;

	
    function closeModal(){
        $uibModalInstance.dismiss("closing");            
    };

    function setAssumptionDialog(assumptionToAssignId){
            AssumptionService.storeAssumption(vm.externalAssumptionDialog[assumptionToAssignId]);
            AssumptionModalService.openAssumptionModal();
    }

    function addAssumptionToMeasurementGoal(obj){
    	console.log("Adding item");
    	if (MeasurementGoalService.addSomethingToMeasurementGoal('Assumption',obj)){
    		$window.alert('Item added');
    	}
    	else {
    		$window.alert('You cannot add this item');
    	}
    	
    }

    /*ctrl.editQuestion = function(question){
        $uibModalInstance.dismiss("closing");
        QuestionStorageFactory.setQuestion(question);
        $location.path('/questionUpdate');
    };*/


}

})();
