(function() {'use strict';

/************************************************************************************
* @ngdoc controller
* @name MetricModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires MetricService
* @requires $uibModalInstance
* @description
* Manages the Metric Modals.
* Realizes the control layer for `modal.metric`.
************************************************************************************/
angular.module('metricapp')

.controller('MetricModalCtrl', MetricModalCtrl);

MetricModalCtrl.$inject = ['$window', '$uibModal', 'MetricService', '$uibModalInstance'];

function MetricModalCtrl($window, $uibModal, MetricService, $uibModalInstance) {

	var vm = this;

	vm.metricDialog = MetricService.getMetricDialog();
	vm.closeModal = closeModal;

    function closeModal(){
        $uibModalInstance.dismiss("closing");            
    };

}

})();
