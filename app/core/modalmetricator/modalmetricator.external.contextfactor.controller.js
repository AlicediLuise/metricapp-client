(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc controller
* @name ExternalContextFactorModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires MeasurementGoalService
* @requires ContextFactorService
* @requires ContextFactorModalService
* @requires $uibModalInstance
* @description
* Manages the External ContextFactor Modals.
* Realizes the control layer for `modal.external.contextfactor`.
************************************************************************************/
.controller('ExternalContextFactorModalCtrl', ExternalContextFactorModalCtrl);

ExternalContextFactorModalCtrl.$inject = ['$window', '$uibModal', 'ContextFactorService', '$uibModalInstance', 'ContextFactorModalService', 'MeasurementGoalService'];

function ExternalContextFactorModalCtrl($window, $uibModal, ContextFactorService, $uibModalInstance, ContextFactorModalService, MeasurementGoalService) {

	var vm = this;

	vm.externalContextFactorDialog = ContextFactorService.getExternalContextFactorDialog();
    
	vm.closeModal = closeModal;
	vm.setContextFactorDialog = setContextFactorDialog;
	vm.addContextFactorToMeasurementGoal = addContextFactorToMeasurementGoal;

	
    function closeModal(){
        $uibModalInstance.dismiss("closing");            
    };

    function setContextFactorDialog(contextFactorToAssignId){
            ContextFactorService.storeContextFactor(vm.externalContextFactorDialog[contextFactorToAssignId]);
            ContextFactorModalService.openContextFactorModal();
    }

    function addContextFactorToMeasurementGoal(obj){
    	console.log("Adding item");
    	if (MeasurementGoalService.addSomethingToMeasurementGoal('ContextFactor',obj)){
    		$window.alert('Item added');
    	}
    	else {
    		$window.alert('You cannot add this item');
    	}
    	
    }

    /*ctrl.editQuestion = function(question){
        $uibModalInstance.dismiss("closing");
        QuestionStorageFactory.setQuestion(question);
        $location.path('/questionUpdate');
    };*/


}

})();
