(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc controller
* @name MeasurementGoalModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires MeasurementGoalService
* @requires $uibModalInstance
* @requires AuthService
* @requires $location 
* @description
* Manages the Measurement Goal Modals.
* Realizes the control layer for `modal.measurementgoal`.
************************************************************************************/
.controller('MeasurementGoalModalCtrl', MeasurementGoalModalCtrl);

MeasurementGoalModalCtrl.$inject = ['$window', '$uibModal', 'MeasurementGoalService', '$uibModalInstance', 'AuthService', '$location', 'StateMachineMeasurementGoalService'];

function MeasurementGoalModalCtrl($window, $uibModal, MeasurementGoalService, $uibModalInstance, AuthService, $location,StateMachineMeasurementGoalService) {

	var vm = this;

	vm.measurementGoalDialog = MeasurementGoalService.getUpdateMeasurementGoal().measurementGoal;
    vm.metrics = MeasurementGoalService.getUpdateMeasurementGoal().metrics;
	vm.contextFactors = MeasurementGoalService.getUpdateMeasurementGoal().contextFactors;
	vm.assumptions = MeasurementGoalService.getUpdateMeasurementGoal().assumptions;
	vm.organizationalGoal = MeasurementGoalService.getUpdateMeasurementGoal().organizationalGoal;
	vm.instanceProject = MeasurementGoalService.getUpdateMeasurementGoal().instanceProject;
	vm.questions = MeasurementGoalService.getUpdateMeasurementGoal().questions;
    vm.isModifiable = isModifiable;
    vm.isForApproval = isForApproval;
    vm.isToReject = isToReject;
    vm.isToChangeRequest = isToChangeRequest;
    vm.isToApprove = isToApprove;
    vm.isToAssign = isToAssign;
    vm.noMetricatorsForMG = noMetricatorsForMG;
    vm.noQuestionersForMG = noQuestionersForMG;
    vm.closeModal = closeModal;
    vm.goToUpdateMeasurementGoal = goToUpdateMeasurementGoal;
    vm.changeState = changeState;
    vm.isToAddQuestioner = isToAddQuestioner;

    function closeModal(){
        $uibModalInstance.dismiss("closing");            
    };

    /********************************************************************************
    * @ngdoc method
    * @name isModifiable
    * @description
    * Measurement Goal can be updated.
    ********************************************************************************/
    function isModifiable(){
        return StateMachineMeasurementGoalService.isModifiable();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isForApproval
    * @description
    * Measurement Goal can be sent for approval.
    ********************************************************************************/ 
    function isForApproval(){
        return StateMachineMeasurementGoalService.isForApproval();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToReject
    * @description
    * Measurement Goal can be rejected.
    ********************************************************************************/ 
    function isToReject(){
        return StateMachineMeasurementGoalService.isToReject();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToApproved
    * @description
    * Measurement Goal can be approved.
    ********************************************************************************/ 
    function isToApprove(){
        return StateMachineMeasurementGoalService.isToApprove();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToChangeRequest
    * @description
    * Measurement Goal can be affected by change request.
    ********************************************************************************/ 
    function isToChangeRequest(){
        return StateMachineMeasurementGoalService.isToChangeRequest();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAssign
    * @description
    * Measurement Goal can be assigned.
    ********************************************************************************/ 
    function isToAssign(){
        return StateMachineMeasurementGoalService.isToAssign();
    }

    /********************************************************************************
    * @ngdoc method
    * @name isToAssign
    * @description
    * Can add a Questioner to a Measurement Goal.
    ********************************************************************************/ 
    function isToAddQuestioner(){
        return StateMachineMeasurementGoalService.isToAddQuestioner();
    }

    /********************************************************************************
    * @ngdoc method
    * @name noMetricatorsForMG
    * @description
    * This function checks if a Measurement Goal contains a metricatorId.
    ********************************************************************************/ 
    function noMetricatorsForMG(){
        return StateMachineMeasurementGoalService.noMetricatorsForMG();
    }

    /********************************************************************************
    * @ngdoc method
    * @name noQuestionersForMG
    * @description
    * This function checks if a Measurement Goal contains a questionerId.
    ********************************************************************************/ 
    function noQuestionersForMG(){   
        return StateMachineMeasurementGoalService.noQuestionersForMG();
    }

    /********************************************************************************
    * @ngdoc method
    * @name goToUpdateMeasurementGoal
    * @description
    * Go to update a Measurement Goal.
    ********************************************************************************/ 
    function goToUpdateMeasurementGoal(){
    	closeModal();
    	$location.path('/measurementgoal');
    }

    /********************************************************************************
    * @ngdoc method
    * @name changeState
    * @description
    * Send MeasurementGoal to change state.
    ********************************************************************************/ 
    function changeState(){
        closeModal();
        $location.path('/measurementgoalchangestate');
    }

}

})();
