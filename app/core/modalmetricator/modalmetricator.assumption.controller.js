(function() {'use strict';

angular.module('metricapp')
/************************************************************************************
* @ngdoc controller
* @name AssumptionModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires AssumptionService
* @description
* Manages the Assumption Modals.
* Realizes the control layer for `modal.assumption`.
************************************************************************************/
.controller('AssumptionModalCtrl', AssumptionModalCtrl);

AssumptionModalCtrl.$inject = ['$window', '$uibModal', 'AssumptionService', '$uibModalInstance'];

function AssumptionModalCtrl($window, $uibModal, AssumptionService, $uibModalInstance) {

	var vm = this;

	vm.assumptionDialog = AssumptionService.getAssumptionDialog();
    vm.setAssumptionDialog = setAssumptionDialog;
    //vm.addAssumptionToMeasurementGoal = setAssumptionDialog;

    vm.closeModal = function(){
        $uibModalInstance.dismiss("closing");            
    };

    function setAssumptionDialog(assumptionToAssignId){
            AssumptionService.storeAssumption(vm.externalAssumptionDialog[assumptionToAssignId]);
            AssumptionModalService.openAssumptionModal();
    }

    /*function addAssumptionToMeasurementGoal(obj){
        console.log("Adding item");
        if (MeasurementGoalService.addSomethingToMeasurementGoal('Assumption',obj)){
            $window.alert('Item added');
        }
        else {
            $window.alert('You cannot add this item');
        }
        
    }*/

    /*ctrl.editQuestion = function(question){
        $uibModalInstance.dismiss("closing");
        QuestionStorageFactory.setQuestion(question);
        $location.path('/questionUpdate');
    };*/


}

})();
