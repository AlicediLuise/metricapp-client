(function() {'use strict';

angular.module('metricapp')

/************************************************************************************
* @ngdoc controller
* @name ExternalMetricModalCtrl
* @module metricapp
* @requires $window
* @requires $uibModal
* @requires MeasurementGoalService
* @requires MetricService
* @requires MetricModalService
* @requires $uibModalInstance
* @description
* Manages the Metric Modals.
* Realizes the control layer for `modal.external.metric`.
************************************************************************************/
.controller('ExternalMetricModalCtrl', ExternalMetricModalCtrl);

ExternalMetricModalCtrl.$inject = ['$window', '$uibModal', 'MetricService', '$uibModalInstance', 'MetricModalService', 'MeasurementGoalService', 'className', 'metricInstance'];

function ExternalMetricModalCtrl($window, $uibModal, MetricService, $uibModalInstance, MetricModalService, MeasurementGoalService, className, metricInstance) {

	var vm = this;

	vm.externalMetricDialog = MetricService.getExternalMetricDialog();

	vm.closeModal = closeModal;
	vm.setMetricDialog = setMetricDialog;
    vm.addMetric = addMetric;
	vm.addMetricToMeasurementGoal = addMetricToMeasurementGoal;

	
    function closeModal(){
        $uibModalInstance.dismiss("closing");            
    };

    function setMetricDialog(metricToAssignId){
            MetricService.storeMetric(vm.externalMetricDialog[metricToAssignId]);
            MetricModalService.openMetricModal();
    }

    function addMetric(obj) {
        if(className === 'Metric')
            addMetricToMetric(obj, metricInstance);
        else
            addMetricToMeasurementGoal(obj);
    }
 
    function addMetricToMeasurementGoal(obj){
    	console.log("Adding item");
    	if (MeasurementGoalService.addSomethingToMeasurementGoal('Metric',obj)){
    		$window.alert('Item added');
    	}
    	else {
    		$window.alert('You cannot add this item');
    	}
    	
    }
 
    function addMetricToMetric(obj, metricInstance){
        console.log("Adding item");
        if (MetricService.addSomethingToMetric('Metric',obj, metricInstance)){
            $window.alert('Item added');
        }
        else {
            $window.alert('You cannot add this item');
        }
 
    }


}

})();
